drop table if exists messages_readied_by cascade;

drop table if exists messages_attachments cascade;

drop table if exists message cascade;

drop table if exists new_messages_in_chat cascade;

drop table if exists chat_notifications_config cascade;

drop table if exists user_chat cascade;

drop table if exists chat cascade;

drop table if exists answer_result cascade;

drop table if exists answer cascade;

drop table if exists confirmation_info cascade;

drop table if exists cors_info cascade;

drop table if exists hibernate_sequence cascade;

drop table if exists moders_of_subjects cascade;

drop table if exists notifications_params cascade;

drop table if exists notification cascade;

drop table if exists password_change_info cascade;

drop table if exists question_result cascade;

drop table if exists question cascade;

drop table if exists test_completing_info cascade;

drop table if exists test_result cascade;

drop table if exists user_allowed_test cascade;

drop table if exists test cascade;

drop table if exists subject cascade;

drop table if exists user_group cascade;

drop table if exists user_role cascade;

drop table if exists role cascade;

drop table if exists user cascade;

drop table if exists group_ cascade;

drop table if exists image cascade;
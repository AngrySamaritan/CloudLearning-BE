create table if not exists cors_info
(
    id        int auto_increment
        primary key,
    allowed   bit          not null,
    host_name varchar(255) null
);

create table if not exists hibernate_sequence
(
    next_val bigint null
);

create table if not exists role
(
    role_id   int          not null
        primary key,
    role_name varchar(255) null
);

create table if not exists subject
(
    subject_id int auto_increment
        primary key,
    name       varchar(255) null
);

create table if not exists group_
(
    id   int auto_increment
        primary key,
    code varchar(255) null
);

create table if not exists image
(
    id_      varchar(255) not null
        primary key,
    bytes    longblob     null,
    name     varchar(255) null,
    hashcode int          not null
);

create table if not exists user
(
    user_id           bigint       not null
        primary key,
    email             varchar(255) null,
    first_name        varchar(255) null,
    last_name         varchar(255) null,
    password          varchar(255) null,
    total_points      bigint       null,
    username          varchar(255) null,
    group_id          int          null,
    profile_image_id_ varchar(255) null,
    constraint FK5ixd8ou7x5sln7b00u8qpf7il
        foreign key (group_id) references group_ (id),
    constraint FKkv4n3txnna2wg5nntx5n5up0e
        foreign key (profile_image_id_) references image (id_)
);

create table if not exists notification
(
    id                 varchar(255) not null
        primary key,
    status             int          null,
    type               varchar(255) null,
    user_id            bigint       null,
    creation_timestamp datetime     not null,
    update_timestamp   datetime     null,
    constraint FKb0yvoep4h4k92ipon31wmdf7e
        foreign key (user_id) references user (user_id)
);

create table if not exists notifications_params
(
    id    varchar(255) not null,
    value varchar(255) null,
    param varchar(255) not null,
    primary key (id, param),
    constraint FKdp07jtuhjbg9wc250hh4g8c5c
        foreign key (id) references notification (id)
);

create table if not exists confirmation_info
(
    id                bigint       not null
        primary key,
    confirmation_code varchar(255) null,
    delete_date       date         null,
    delete_time       time         null,
    user_user_id      bigint       null,
    constraint FKry3w7ivtr1rh04k5om83152g7
        foreign key (user_user_id) references user (user_id)
);

create table if not exists moders_of_subjects
(
    subject_id int    not null,
    user_id    bigint not null,
    constraint FKdgrmnb7fn977k7iu116uk0bbg
        foreign key (subject_id) references subject (subject_id),
    constraint FKitany2xqnsjc7fq0kwe9ji4yj
        foreign key (user_id) references user (user_id)
);

create table if not exists password_change_info
(
    id                bigint auto_increment
        primary key,
    confirmation_code varchar(255) null,
    invalidate_date   date         null,
    invalidate_time   time         null,
    user_id           bigint       null,
    constraint FK7xbn8murkpl6x8aax35ach1ul
        foreign key (user_id) references user (user_id)
);

create table if not exists test
(
    test_id           bigint auto_increment
        primary key,
    duration          int          not null,
    max_points        int          null,
    name              varchar(255) null,
    test_description  varchar(255) null,
    author_id         bigint       null,
    subject_id        int          null,
    marked_for_delete bit          not null,
    constraint FKrexpoyol6jlce2a6rfwtg3er8
        foreign key (subject_id) references subject (subject_id),
    constraint FKri5vnwoi057j5f2a6angur1xp
        foreign key (author_id) references user (user_id)
);

create table if not exists question
(
    question_id   bigint       not null
        primary key,
    points_amount int          null,
    question_text varchar(255) null,
    question_type varchar(255) null,
    test_id       bigint       null,
    constraint FK8hejcpbbiq1qje11346akp3uj
        foreign key (test_id) references test (test_id)
);

create table if not exists answer
(
    answer_id varchar(255) not null
        primary key,
    text      varchar(255) null,
    is_right  bit          null,
    question  bigint       null,
    constraint FKs4yfxjrfvek48fcsvg9ndcktu
        foreign key (question) references question (question_id)
);

create table if not exists test_completing_info
(
    id              int auto_increment
        primary key,
    invalidate_time time   null,
    test_id         bigint null,
    user_id         bigint null,
    constraint FKixxkqu6byp71u6uo4xud5t4cl
        foreign key (user_id) references user (user_id),
    constraint FKjf7t3i92h9r846o594kt2qixm
        foreign key (test_id) references test (test_id)
);

create table if not exists test_result
(
    result_id           bigint not null
        primary key,
    completion_date     date   null,
    completion_duration time   null,
    completion_time     time   null,
    points              bigint not null,
    test_id             bigint null,
    user_id             bigint null,
    constraint FKef3e8k7fgvkj4mox0lxrkf8hh
        foreign key (test_id) references test (test_id),
    constraint FKmu7x4i4r6swolpxwqx0n21lnn
        foreign key (user_id) references user (user_id)
);

create table if not exists question_result
(
    result_id            bigint not null
        primary key,
    points_amount        int    not null,
    question_question_id bigint null,
    test_result          bigint null,
    constraint FKdwc5gx1hb155fjq649vli50o5
        foreign key (test_result) references test_result (result_id),
    constraint FKrt7us627v7u5r2cevwuqd4cwg
        foreign key (question_question_id) references question (question_id)
);

create table if not exists answer_result
(
    answer_result_id   bigint       not null
        primary key,
    control_result     varchar(255) null,
    answer_id          varchar(255) null,
    question_result_id bigint       null,
    constraint FKef1jxh8bmycekdl4nn8hrefr5
        foreign key (answer_id) references answer (answer_id),
    constraint FKoo656j2fadcpbqttl7lasxxsq
        foreign key (question_result_id) references question_result (result_id)
);

create table if not exists user_allowed_test
(
    allowed_test_id bigint not null,
    allowed_user_id bigint not null,
    constraint FK458529a9jihth6go6bx43c4xa
        foreign key (allowed_user_id) references user (user_id),
    constraint FKr66uc92orb9805x9vt7j80u5p
        foreign key (allowed_test_id) references test (test_id)
);

create table if not exists user_role
(
    user_id bigint not null,
    role_id int    not null,
    constraint FK859n2jvi8ivhui0rl0esws6o
        foreign key (user_id) references user (user_id),
    constraint FKa68196081fvovjhkek5m97n3y
        foreign key (role_id) references role (role_id)
);



INSERT INTO cors_info (id, allowed, host_name)
VALUES (1, true, 'http://localhost:3000');

INSERT INTO role (role_id, role_name)
VALUES (-1, 'ROLE_UNCONFIRMED');
INSERT INTO role (role_id, role_name)
VALUES (0, 'ROLE_USER');
INSERT INTO role (role_id, role_name)
VALUES (1, 'ROLE_MODER');
INSERT INTO role (role_id, role_name)
VALUES (2, 'ROLE_ADMIN');

INSERT INTO group_ (id, code)
VALUES (1, '10702118');
INSERT INTO group_ (id, code)
VALUES (2, '1');
INSERT INTO group_ (id, code)
VALUES (3, '12345');
INSERT INTO group_ (id, code)
VALUES (4, '1234');

INSERT INTO user (user_id, email, first_name, last_name, password, total_points, username, group_id,
                  profile_image_id_)
VALUES (1, 'andrey-truhan@tut.by', 'Andrei', 'Trukhan', '$2a$10$t0GHJ.QLbL0tSWMdeVQYhuQGgnbtr.kkcYSivezBrd1.760yf1Nt2',
        0, 'Andrei1107', 1, null);
INSERT INTO user (user_id, email, first_name, last_name, password, total_points, username, group_id,
                  profile_image_id_)
VALUES (15, 'demo_admin@mockmail.com', 'firstName', 'lastName',
        '$2a$10$WQ2lcDaYRQDP/HGPFjSH1.coCOi3we0VAG.O9rAEJD59n8xCUG6ka', 0, 'demo_admin', 3,
        null);
INSERT INTO user (user_id, email, first_name, last_name, password, total_points, username, group_id,
                  profile_image_id_)
VALUES (44, 'demo_user@mockmail.com', 'Demo', 'User', '$2a$10$bQIswxVR3L6xPosbYUUBHO78Rx8zGuxRByYjMhMelrM/00.8B68Cu', 0,
        'demo_user', 4, null);
INSERT INTO user (user_id, email, first_name, last_name, password, total_points, username, group_id,
                  profile_image_id_)
VALUES (46, 'demo_moder@mockmail.com', 'Demo', 'Moder', '$2a$10$Vft4BMSaaMcVGhTI56shs.TRWVZGmCVgDZ1kUjVWzAKnSeGU5iEVO',
        0, 'demo_moder', 3, null);

INSERT INTO user_role (user_id, role_id)
VALUES (1, 0);
INSERT INTO user_role (user_id, role_id)
VALUES (1, 1);
INSERT INTO user_role (user_id, role_id)
VALUES (1, 2);

INSERT INTO user_role (user_id, role_id)
VALUES (15, 0);
INSERT INTO user_role (user_id, role_id)
VALUES (15, 1);
INSERT INTO user_role (user_id, role_id)
VALUES (15, 2);

INSERT INTO user_role (user_id, role_id)
VALUES (44, 0);

INSERT INTO user_role (user_id, role_id)
VALUES (46, 0);
INSERT INTO user_role (user_id, role_id)
VALUES (46, 1);


INSERT INTO subject(subject_id, name)
VALUES (1, 'ООП');

INSERT INTO subject(subject_id, name)
VALUES (2, 'Безопасность');

INSERT INTO moders_of_subjects(user_id, subject_id)
VALUES (1, 1);
INSERT INTO moders_of_subjects(user_id, subject_id)
VALUES (15, 1);
INSERT INTO moders_of_subjects(user_id, subject_id)
VALUES (1, 2);
INSERT INTO moders_of_subjects(user_id, subject_id)
VALUES (46, 2);

INSERT INTO hibernate_sequence(next_val)
VALUES (0);
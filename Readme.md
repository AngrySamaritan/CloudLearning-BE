<h1> Cloud learning </h1>

************

<h3> Description </h3>

This system allows moderators to create tests and users to complete tests.

<b>Back-end:</b> Spring Boot Rest API, MYSQL, Hibernate.

<b>Front-end:</b> ReactJS, HTML5, CSS3.

<b>Features:</b>
<ul>
  <li>JWT based authentication. </li>
  <li>Users mail confirmation. </li>
  <li>Test completing time control on server side.</li>
</ul>

************

<h3> Screenshots </h3>



<h5>Home Page</h5>

![Home Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/HomePage.png)


<h5>Profile Page</h5>

![Home Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/profile%20page.png)


<h5>Subject Page</h5>

![Home Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/Subject%20Page.png)

<h5>Test Create Page</h5>

![Test Create Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/TestCreatePage.png)

<h5>Test Complete Page</h5>

![Test Complete Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/TestCompletePage.png)

<h5>Results Page</h5>

![Results Page](https://github.com/AngrySamaritan/webTestSystem/blob/master/ScreenShots/Result%20Page.png)

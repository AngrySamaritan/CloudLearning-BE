package com.angrysamaritan.webtestsystem.util;

import com.angrysamaritan.webtestsystem.dto.results.AnswerResultDto;
import com.angrysamaritan.webtestsystem.model.AnswerResult;

import java.util.LinkedList;
import java.util.List;

public class AnswerResultMapper {

    public static List<AnswerResultDto> makeResultsList(List<AnswerResult> answerResults) {
        var resultDtoList = new LinkedList<AnswerResultDto>();
        for (var answerResult : answerResults) {
            resultDtoList.add(new AnswerResultDto(answerResult));
        }
        return resultDtoList;
    }

}

package com.angrysamaritan.webtestsystem.util;

import com.angrysamaritan.webtestsystem.model.AnswerResult;
import com.angrysamaritan.webtestsystem.model.ControlResult;
import com.angrysamaritan.webtestsystem.model.Question;
import com.angrysamaritan.webtestsystem.model.QuestionResult;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class DefaultPointCountStrategy implements PointCountStrategy {
    @Override
    public Integer countPointsMultipleSelect(Question question, QuestionResult questionResult) {
        return countSelect(question, questionResult);
    }

    @Override
    public Integer countPointsSingleSelect(Question question, QuestionResult questionResult) {
        return countSelect(question, questionResult);
    }

    private Integer countSelect(Question question, QuestionResult questionResult) {
        if (CollectionUtils.isEmpty(questionResult.getAnswers())
                || questionResult.getAnswers().stream().map(AnswerResult::getControlResult)
                .anyMatch(controlResult -> controlResult == ControlResult.WRONG_UNCHECKED
                        || controlResult == ControlResult.WRONG_CHECKED)) {
            return 0;
        }
        return question.getPointsAmount();
    }

    @Override
    public Integer countPointsWriting(Question question, QuestionResult questionResult) {
        if (CollectionUtils.isEmpty(questionResult.getAnswers())) {
            return 0;
        }
        ControlResult controlResult = questionResult.getAnswers().get(0).getControlResult();
        if (controlResult == ControlResult.RIGHT_UNCHECKED) {
            return 0;
        }
        return question.getPointsAmount();
    }
}

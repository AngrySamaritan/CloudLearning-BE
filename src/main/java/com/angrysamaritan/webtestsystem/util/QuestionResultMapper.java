package com.angrysamaritan.webtestsystem.util;

import com.angrysamaritan.webtestsystem.dto.results.QuestionResultDto;
import com.angrysamaritan.webtestsystem.model.QuestionResult;

import java.util.LinkedList;
import java.util.List;

public class QuestionResultMapper {

    public static List<QuestionResultDto> makeResultsList(List<QuestionResult> questionResults) {
        var resultDtoList = new LinkedList<QuestionResultDto>();
        for (var questionResult: questionResults) {
            resultDtoList.add(new QuestionResultDto(questionResult));
        }
        return resultDtoList;
    }
}

package com.angrysamaritan.webtestsystem.util;

import com.angrysamaritan.webtestsystem.model.Question;
import com.angrysamaritan.webtestsystem.model.QuestionResult;

public interface PointCountStrategy {
    Integer countPointsMultipleSelect(Question question, QuestionResult questionResult);

    Integer countPointsSingleSelect(Question question, QuestionResult questionResult);

    Integer countPointsWriting(Question question, QuestionResult questionResult);
}

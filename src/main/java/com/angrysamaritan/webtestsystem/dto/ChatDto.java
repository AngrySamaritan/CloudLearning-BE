package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import lombok.*;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {

    private String id;

    private String name;

    private String imageId;

    private List<ProfileDtoResp> participants;

    private List<Long> participantsIds;

    private int newMessagesAmount;

    private boolean notificationsEnabled;
}

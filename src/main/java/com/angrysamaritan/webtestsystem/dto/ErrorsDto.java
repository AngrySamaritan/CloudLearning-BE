package com.angrysamaritan.webtestsystem.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Getter
public class ErrorsDto {

    public ErrorsDto(Errors errors) {
        if (errors.hasErrors()) {
            errors.getFieldErrors().forEach(error -> this.errors.put(error.getField(), error.getDefaultMessage()));
        }
        if (errors.hasGlobalErrors()) {
            this.errors.put("passwordMatches", Objects.requireNonNull(errors.getGlobalError()).getDefaultMessage());
        }
    }

    private final Map<String, String> errors = new HashMap<>();

}

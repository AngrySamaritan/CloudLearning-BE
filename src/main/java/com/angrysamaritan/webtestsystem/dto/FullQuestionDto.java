package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.model.Question;
import com.angrysamaritan.webtestsystem.model.QuestionType;
import com.angrysamaritan.webtestsystem.validators.HasRightAnswers;
import com.angrysamaritan.webtestsystem.validators.ValidAnswersAmount;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ValidAnswersAmount
@HasRightAnswers
@Builder
public class FullQuestionDto {

    @JsonProperty("id")
    private Long questionId;

    @JsonProperty("text")
    @NotBlank(message = "Question text must not be blank")
    private String questionText;

    @JsonProperty("points")

    @Min(1)
    private Integer pointsAmount;

    @JsonProperty("type")
    @NotNull
    private QuestionType questionType;

    private List<@Valid FullAnswerDto> answers;

    private String imageId;

    public FullQuestionDto(Question question) {
        this(question.getQuestionId(), question.getQuestionText(), question.getPointsAmount(), question.getQuestionType(),
                question.getAnswers().stream().map(FullAnswerDto::new).collect(Collectors.toList()),
                Optional.ofNullable(question.getImage()).orElse(new Image()).getId());
    }
}

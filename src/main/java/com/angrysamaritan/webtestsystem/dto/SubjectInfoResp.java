package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.model.Subject;
import com.angrysamaritan.webtestsystem.model.User;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.LinkedList;
import java.util.List;

@Value
@AllArgsConstructor
public class SubjectInfoResp {

    public SubjectInfoResp(Subject subject) {
        id = subject.getSubjectId();
        name = subject.getName();
        for (User teacher : subject.getTeachers()) {
            teachers.add(new UserInfoResp(teacher));
        }
    }

    long id;

    String name;

    List<UserInfoResp> teachers = new LinkedList<>();

}

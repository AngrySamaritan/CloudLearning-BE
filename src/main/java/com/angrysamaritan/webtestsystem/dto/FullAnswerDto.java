package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.model.Answer;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullAnswerDto {

    @JsonProperty("id")
    private String answerId;

    @JsonProperty("text")
    @NotBlank(message = "Test description must not be blank")
    private String answerText;

    @JsonProperty("isTrue")
    private boolean isRight;

    public FullAnswerDto(Answer answer) {
        this(answer.getAnswerId(), answer.getAnswerText(), answer.isRight());
    }
}

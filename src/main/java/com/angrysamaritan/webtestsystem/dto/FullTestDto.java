package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.model.Test;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FullTestDto {

    public FullTestDto(Test test) {
        this(test.getTestId(), test.getName(), test.getDuration(), test.getTestDescription(),
                test.getQuestions().stream().map(FullQuestionDto::new).collect(Collectors.toList()),
                test.getSubject().getSubjectId());
    }

    @JsonProperty("id")
    private Long testId;

    @NotBlank(message = "Test name must not be blank")
    private String name;

    @Min(value = 1, message = "Test duration must be not less than 1 minute")
    @Max(value = 120,  message = "Test duration must be not more than 2 hours")
    private int duration;

    @JsonProperty("description")
    @NotBlank(message = "Test description must not be blank")
    private String testDescription;

    private List<@Valid FullQuestionDto> questions;

    private int subjectId;
}

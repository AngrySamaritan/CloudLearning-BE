package com.angrysamaritan.webtestsystem.dto.group;

import com.angrysamaritan.webtestsystem.model.Group;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class GroupInfoResp {

    public GroupInfoResp(Group group) {
        id = group.getId();
        code = group.getCode();
    }

    long id;

    String code;
}

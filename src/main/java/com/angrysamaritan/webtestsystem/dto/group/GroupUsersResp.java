package com.angrysamaritan.webtestsystem.dto.group;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;

import java.util.List;

@Value
public class GroupUsersResp  {

    long id;

    String code;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    List<UserInfoResp> users;
}

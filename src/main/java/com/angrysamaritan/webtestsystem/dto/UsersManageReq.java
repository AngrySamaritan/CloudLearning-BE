package com.angrysamaritan.webtestsystem.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class UsersManageReq {

    private List<Long> putUsers;

    private List<Long> deleteUsers;

}

package com.angrysamaritan.webtestsystem.dto.user;

public interface IPasswordWithConfirmation {
    String getPassword();

    String getPasswordConfirmation();

}

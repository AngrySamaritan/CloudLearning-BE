package com.angrysamaritan.webtestsystem.dto.user;

import com.angrysamaritan.webtestsystem.dto.SubjectInfoResp;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.model.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Value
@AllArgsConstructor
public class ProfileDtoResp {

    public ProfileDtoResp(User user) {
        this(user.getUserId(), user.getFirstName(), user.getLastName(), user.getUsername(),
                user.getGroup().getCode(), user.getGroup().getId(), user.getEmail(),
                Optional.ofNullable(user.getProfileImage()).isPresent() ? user.getProfileImage().getId() : null,
                user.getRoles().stream().map(Role::getRoleName).map(Enum::name).collect(Collectors.toList()),
                user.getAssignedSubjects().stream().map(SubjectInfoResp::new).collect(Collectors.toList()));
    }

    long id;

    String firstName;

    String lastName;

    String username;

    String group;

    long groupId;

    String email;

    String imageId;

    List<String> roles;

    List<SubjectInfoResp> assignedSubjects;
}

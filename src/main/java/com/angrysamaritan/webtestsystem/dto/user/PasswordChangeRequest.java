package com.angrysamaritan.webtestsystem.dto.user;

import com.angrysamaritan.webtestsystem.validators.PasswordMatches;
import com.angrysamaritan.webtestsystem.validators.StrongPassword;
import lombok.*;

@PasswordMatches
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PasswordChangeRequest implements IPasswordWithConfirmation {

    @StrongPassword
    private String password;

    private String passwordConfirmation;

}

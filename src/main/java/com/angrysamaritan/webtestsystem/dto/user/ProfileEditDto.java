package com.angrysamaritan.webtestsystem.dto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@NoArgsConstructor
@Getter
public class ProfileEditDto {

    private String firstName;

    private String lastName;

    private String group;

    private String imageId;
}

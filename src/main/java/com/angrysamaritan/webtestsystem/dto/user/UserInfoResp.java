package com.angrysamaritan.webtestsystem.dto.user;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.model.User;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class UserInfoResp {

    public UserInfoResp(User user) {
        this(user.getUserId(), user.getFirstName(), user.getLastName(),
                new GroupInfoResp(user.getGroup()));
    }

    long id;

    String firstName;

    String lastName;

    GroupInfoResp group;
}

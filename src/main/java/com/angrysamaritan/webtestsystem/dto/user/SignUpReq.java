package com.angrysamaritan.webtestsystem.dto.user;

import com.angrysamaritan.webtestsystem.validators.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@PasswordMatches
@Getter
@Setter
@NoArgsConstructor
public class SignUpReq implements IPasswordWithConfirmation {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    @UniqueUsername
    private String username;

    @UniqueEmail
    @ValidEmail
    private String email;

    private String group;

    @StrongPassword
    private String password;

    private String passwordConfirmation;

    private String confirmationUrl;

}

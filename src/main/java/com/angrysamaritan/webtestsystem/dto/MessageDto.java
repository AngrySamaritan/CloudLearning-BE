package com.angrysamaritan.webtestsystem.dto;

import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.model.Message;
import com.angrysamaritan.webtestsystem.model.User;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {

    public MessageDto(Message message, User user) {
        chatId = message.getChat().getId();
        text = message.getText();
        attachments = message.getAttachments().stream().map(Image::getId).collect(Collectors.toList());
        sender = new ProfileDtoResp(message.getSender());
        isReadied = message.getUsersReadied().contains(user);
        timestamp = message.getTimestamp();
    }

    private String chatId;

    private String text;

    private List<String> attachments;

    @Setter
    private ProfileDtoResp sender;

    private boolean isReadied;

    private Timestamp timestamp;
}

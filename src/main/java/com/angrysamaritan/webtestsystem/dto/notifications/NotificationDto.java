package com.angrysamaritan.webtestsystem.dto.notifications;


import com.angrysamaritan.webtestsystem.model.Notification;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Map;
import java.util.UUID;

@Value
@AllArgsConstructor
public class NotificationDto {

    public NotificationDto(Notification notification) {
        this(notification.getId(), notification.getParams(), notification.getType(), notification.getStatus());
    }

    UUID id;

    Map<String, String> params;

    Notification.NotificationTypes type;

    Notification.NotificationStatus status;

    public enum NotificationTypes {
        ACCESS_TO_TEST_GRANTED,
    }
}

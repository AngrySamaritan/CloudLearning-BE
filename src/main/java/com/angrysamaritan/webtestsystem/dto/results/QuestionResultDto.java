package com.angrysamaritan.webtestsystem.dto.results;

import com.angrysamaritan.webtestsystem.model.QuestionResult;
import com.angrysamaritan.webtestsystem.model.QuestionType;
import com.angrysamaritan.webtestsystem.util.AnswerResultMapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@AllArgsConstructor
public class QuestionResultDto {

    public QuestionResultDto(QuestionResult questionResult) {
        this(questionResult.getResultId(), questionResult.getQuestion().getQuestionText(),
                questionResult.getPointsAmount(), questionResult.getQuestion().getQuestionType(),
                AnswerResultMapper.makeResultsList(questionResult.getAnswers()));
    }

    long id;

    String text;

    long pointsAmount;

    @JsonProperty("type")
    QuestionType questionType;

    List<AnswerResultDto> answers;
}

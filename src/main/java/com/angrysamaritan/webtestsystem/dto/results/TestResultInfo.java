package com.angrysamaritan.webtestsystem.dto.results;

import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.model.TestResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Date;

@Value
@AllArgsConstructor
public class TestResultInfo {

    public TestResultInfo(TestResult testResult) {
        this(testResult.getResultId(), testResult.getTest().getName(), testResult.getCompletionDate(),
                Math.round((double) testResult.getPoints() / testResult.getTest().getMaxPoints() * 10));
    }

    long id;

    String testName;

    @JsonFormat(pattern = "dd.MM.yyyy")
    Date completionDate;

    long mark;

}

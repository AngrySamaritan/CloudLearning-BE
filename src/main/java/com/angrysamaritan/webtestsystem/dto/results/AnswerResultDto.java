package com.angrysamaritan.webtestsystem.dto.results;

import com.angrysamaritan.webtestsystem.model.AnswerResult;
import com.angrysamaritan.webtestsystem.model.ControlResult;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class AnswerResultDto {

    public AnswerResultDto(AnswerResult answerResult) {
        this(answerResult.getAnswerResultId(), answerResult.getAnswer().getAnswerText(), answerResult.getControlResult());
    }

    long id;

    String text;

    ControlResult controlResult;
}

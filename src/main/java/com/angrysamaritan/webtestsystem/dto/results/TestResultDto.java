package com.angrysamaritan.webtestsystem.dto.results;

import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.model.TestResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Date;
import java.sql.Time;

@Getter
@AllArgsConstructor
public class TestResultDto {

    public TestResultDto(TestResult testResult) {
        this(testResult.getResultId(), new UserInfoResp(testResult.getUser()),
                new TestInfoResp(testResult.getTest()), testResult.getCompletionDate(), testResult.getCompletionTime(),
                testResult.getCompletionDuration(), Math.round(testResult.getPoints() * 1. / testResult.getTest().getMaxPoints()));
    }

    long id;

    UserInfoResp user;

    TestInfoResp test;

    @JsonProperty("date")
    @JsonFormat(pattern="yyyy-MM-dd")
    Date completionDate;

    @JsonProperty("time")
    Time completionTime;

    Time completionDuration;

    long mark;
}

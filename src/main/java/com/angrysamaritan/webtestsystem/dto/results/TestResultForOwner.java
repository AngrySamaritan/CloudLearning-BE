package com.angrysamaritan.webtestsystem.dto.results;

import com.angrysamaritan.webtestsystem.model.TestResult;
import com.angrysamaritan.webtestsystem.util.QuestionResultMapper;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.List;

@Value
@EqualsAndHashCode(callSuper = true)
public class TestResultForOwner extends TestResultDto {

    public TestResultForOwner(TestResult testResult) {
        super(testResult);
        this.questions = QuestionResultMapper.makeResultsList(testResult.getQuestionResults());
    }


    List<QuestionResultDto> questions;

}

package com.angrysamaritan.webtestsystem.dto.test;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
public class AnswerDtoReq {

    @NotBlank
    private String text;

    @NotNull
    private Boolean isTrue;
}

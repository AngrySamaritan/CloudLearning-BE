package com.angrysamaritan.webtestsystem.dto.test;

import com.angrysamaritan.webtestsystem.model.Question;
import com.angrysamaritan.webtestsystem.model.Test;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.LinkedList;
import java.util.List;

@Value
@AllArgsConstructor
public class TestDtoResp {

    public TestDtoResp(Test test) {
        name = test.getName();
        duration = test.getDuration();
        id = test.getTestId();
        List<QuestionDtoResp> questions = new LinkedList<>();
        for (Question question : test.getQuestions()) {
            QuestionDtoResp questionDto = new QuestionDtoResp(question);
            questions.add(questionDto);
        }
        this.questions = questions;
    }

    long id;

    String name;

    int duration;

    List<QuestionDtoResp> questions;
}

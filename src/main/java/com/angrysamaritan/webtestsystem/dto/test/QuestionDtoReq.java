package com.angrysamaritan.webtestsystem.dto.test;

import com.angrysamaritan.webtestsystem.model.QuestionType;
import com.angrysamaritan.webtestsystem.validators.HasRightAnswers;
import com.angrysamaritan.webtestsystem.validators.ValidAnswersAmount;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Getter
@ValidAnswersAmount
@HasRightAnswers
public class QuestionDtoReq {

    @NotBlank
    private String text;

    private QuestionType type;

    private List<@Valid AnswerDtoReq> answers;

    @PositiveOrZero
    private int points;
}

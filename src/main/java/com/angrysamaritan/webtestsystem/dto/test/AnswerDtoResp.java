package com.angrysamaritan.webtestsystem.dto.test;

import com.angrysamaritan.webtestsystem.model.Answer;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.UUID;

@Value
@AllArgsConstructor
public class AnswerDtoResp {

    public AnswerDtoResp(Answer answer) {
        id = answer.getAnswerId();
        text = answer.getAnswerText();
    }

    String id;

    String text;

}

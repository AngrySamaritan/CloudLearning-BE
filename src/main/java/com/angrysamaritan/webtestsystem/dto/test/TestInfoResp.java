package com.angrysamaritan.webtestsystem.dto.test;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.model.Test;
import lombok.Value;

@Value
public class TestInfoResp {

    public TestInfoResp(Test test) {
        id = test.getTestId();
        name = test.getName();
        duration = test.getDuration();
        author = new UserInfoResp(test.getAuthor());
        description = test.getTestDescription();
        canEdit = false;
    }

    public TestInfoResp(Test test, boolean canEdit) {
        id = test.getTestId();
        name = test.getName();
        duration = test.getDuration();
        author = new UserInfoResp(test.getAuthor());
        description = test.getTestDescription();
        this.canEdit = canEdit;
    }

    long id;

    String name;

    long duration;

    UserInfoResp author;

    String description;

    boolean canEdit;
}

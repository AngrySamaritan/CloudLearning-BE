package com.angrysamaritan.webtestsystem.dto.test;

import com.angrysamaritan.webtestsystem.model.Answer;
import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.model.Question;
import com.angrysamaritan.webtestsystem.model.QuestionType;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Value
@AllArgsConstructor
public class QuestionDtoResp {

    public QuestionDtoResp(Question question) {
        text = question.getQuestionText();
        type = question.getQuestionType();
        id = question.getQuestionId();
        imageId = Optional.ofNullable(question.getImage()).orElse(new Image()).getId();
        List<AnswerDtoResp> answers = new LinkedList<>();
        for (Answer answer : question.getAnswers()) {
            AnswerDtoResp answerDto = new AnswerDtoResp(answer);
            answers.add(answerDto);
        }
        this.answers = answers;
    }

    long id;

    String text;

    QuestionType type;

    List<AnswerDtoResp> answers;

    String imageId;
}

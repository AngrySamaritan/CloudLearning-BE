package com.angrysamaritan.webtestsystem.dto.test;

import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class TestDtoReq {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @Min(1)
    @Max(300)
    private int duration;


    private List<@Valid QuestionDtoReq> questions;

    @NotNull
    private int subjectId;
}

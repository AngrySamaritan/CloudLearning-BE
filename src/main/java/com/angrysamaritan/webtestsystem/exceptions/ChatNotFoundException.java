package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ChatNotFoundException extends NotFoundException {

    public ChatNotFoundException(String chatId) {
        super(String.format("Chat id = %s not found", chatId));
    }
}

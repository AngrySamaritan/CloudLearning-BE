package com.angrysamaritan.webtestsystem.exceptions;

import com.angrysamaritan.webtestsystem.dto.ErrorsDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ResponseStatus;

@EqualsAndHashCode(callSuper = true)
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
public class BadRequestException extends RuntimeException {

    public BadRequestException(String message) {
        super(message);
        errorsDto = null;
    }

    public BadRequestException(String message, Errors errors) {
        super(message);
        errorsDto = new ErrorsDto(errors);
    }

    private final ErrorsDto errorsDto;
}

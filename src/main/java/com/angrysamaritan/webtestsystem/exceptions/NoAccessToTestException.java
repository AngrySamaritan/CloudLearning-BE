package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoAccessToTestException extends AccessException {
    public NoAccessToTestException(Long id) {
        super("Sorry, test id" + id + " is closed for you");
    }
}

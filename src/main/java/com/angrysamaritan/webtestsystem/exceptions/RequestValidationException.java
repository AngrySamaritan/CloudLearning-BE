package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.validation.Errors;

public class RequestValidationException extends RuntimeException {
    Errors errors;

    public RequestValidationException(Errors errors) {
        this.errors = errors;
    }
}

package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TestCreateException extends RuntimeException {
    public TestCreateException(String e) {
        super(e);
    }
}

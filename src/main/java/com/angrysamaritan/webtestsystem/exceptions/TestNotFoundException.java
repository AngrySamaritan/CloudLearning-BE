package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TestNotFoundException extends NotFoundException {

    public TestNotFoundException(Long id) {
        super("Test id: " + id + " not found!");
    }
}

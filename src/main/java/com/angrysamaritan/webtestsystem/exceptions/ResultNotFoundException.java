package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ResultNotFoundException extends NotFoundException {
    public ResultNotFoundException(long resultId) {
        super("Result with id = " + resultId + " not exist.");
    }
}

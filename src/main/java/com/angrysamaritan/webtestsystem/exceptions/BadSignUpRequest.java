package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.validation.Errors;

public class BadSignUpRequest extends BadRequestException {
    public BadSignUpRequest(Errors errors) {
        super("Bad sign up request", errors);
    }
}

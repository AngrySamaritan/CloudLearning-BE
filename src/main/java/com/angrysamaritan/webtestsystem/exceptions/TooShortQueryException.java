package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TooShortQueryException extends RuntimeException {
    public TooShortQueryException(int minLength) {
        super("Query length is lower than " + minLength);
    }
}

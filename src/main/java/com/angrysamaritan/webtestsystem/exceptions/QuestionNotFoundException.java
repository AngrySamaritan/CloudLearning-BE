package com.angrysamaritan.webtestsystem.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class QuestionNotFoundException extends NotFoundException {
    public QuestionNotFoundException(long id) {
        super(String.format("Question id %d not found!", id));
    }
}

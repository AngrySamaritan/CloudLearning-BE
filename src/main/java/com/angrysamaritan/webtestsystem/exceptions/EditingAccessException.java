package com.angrysamaritan.webtestsystem.exceptions;

public class EditingAccessException extends AccessException {
    public EditingAccessException(long id) {
        super("Editing access to user's id: " + id + " profile denied!");
    }
}

package com.angrysamaritan.webtestsystem.model;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.List;

import static com.angrysamaritan.webtestsystem.config.KeyWords.QUESTION;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long questionId;

    private String questionText;

    private Integer pointsAmount;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;

    @Enumerated(EnumType.STRING)
    private QuestionType questionType;

    @OneToMany(mappedBy = QUESTION)
    @Cascade(CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private List<Answer> answers;

    @ManyToOne
    @JoinColumn(name = "image_id")
    private Image image;
}

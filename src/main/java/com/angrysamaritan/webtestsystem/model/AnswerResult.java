package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class AnswerResult {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long answerResultId;

    @ManyToOne
    @JoinColumn(name = "question_result_id")
    private QuestionResult questionResult;

    @ManyToOne
    @JoinColumn(name = "answer_id")
    private Answer answer;

    @Enumerated(EnumType.STRING)
    private ControlResult controlResult;
}

package com.angrysamaritan.webtestsystem.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import static com.angrysamaritan.webtestsystem.config.KeyWords.QUESTION;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class Answer {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "answer_id")
    @Getter
    @Setter
    private String answerId;

    @Column(name = "text")
    @Getter
    @Setter
    private String answerText;

    @Column(name = "is_right")
    @Getter
    @Setter
    private boolean isRight;

    @ManyToOne
    @Getter
    @Setter
    @JoinColumn(name = QUESTION)
    private Question question;
}

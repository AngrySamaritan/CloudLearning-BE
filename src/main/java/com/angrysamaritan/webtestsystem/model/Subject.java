package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Indexed;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@Getter
@Setter
@EqualsAndHashCode
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int subjectId;

    String name;

    @ManyToMany
    @JoinTable(name = "moders_of_subjects",
            joinColumns = {@JoinColumn(name = "subject_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @EqualsAndHashCode.Exclude
    private List<User> teachers;

    @OneToMany(mappedBy = "subject")
    @EqualsAndHashCode.Exclude
    private List<Test> tests;

    private boolean markedForDelete = false;
}

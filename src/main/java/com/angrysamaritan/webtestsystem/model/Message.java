package com.angrysamaritan.webtestsystem.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
public class Message {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToMany
    @JoinTable(name = "messages_attachments",
            joinColumns = {@JoinColumn(name = "message_id")},
            inverseJoinColumns = {@JoinColumn(name = "attachment_id")}
    )
    private List<Image> attachments;

    private String text;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User sender;

    @CreationTimestamp
    private Timestamp timestamp;

    @ManyToMany
    @JoinTable(name = "messages_readied_by",
            joinColumns = {@JoinColumn(name = "message_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> usersReadied;

    @ManyToOne
    @JoinColumn(name = "chat_id")
    private Chat chat;
}

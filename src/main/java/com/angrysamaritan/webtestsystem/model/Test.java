package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@Getter
@Setter
@EqualsAndHashCode
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long testId;

    @Field(index = Index.YES)
    private String name;

    private int duration;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    private String testDescription;

    @OneToMany(mappedBy = "test")
    @Cascade(CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private List<Question> questions;

    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subject subject;

    private Integer maxPoints;

    @OneToMany(mappedBy = "test")
    @EqualsAndHashCode.Exclude
    private List<TestResult> testResults;

    @ManyToMany
    @JoinTable(name = "user_allowed_test",
            joinColumns = {@JoinColumn(name = "allowed_test_id")},
            inverseJoinColumns = {@JoinColumn(name = "allowed_user_id")})
    @EqualsAndHashCode.Exclude
    private List<User> allowedUsers;

    @EqualsAndHashCode.Exclude
    private boolean markedForDelete = false;
}

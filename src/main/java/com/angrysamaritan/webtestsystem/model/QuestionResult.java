package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode
@Getter
@Setter
public class QuestionResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long resultId;

    @OneToMany(mappedBy = "questionResult")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private List<AnswerResult> answers;

    @ManyToOne
    @JoinColumn(name = "test_result")
    private TestResult testResult;

    @ManyToOne
    private Question question;

    private int pointsAmount;
}

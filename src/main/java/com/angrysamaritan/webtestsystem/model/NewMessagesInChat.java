package com.angrysamaritan.webtestsystem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewMessagesInChat implements Serializable {

    @Id
    @ManyToOne
    private Chat chat;

    @Id
    @ManyToOne
    private User user;

    private long amount;
}

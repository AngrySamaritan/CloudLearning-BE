package com.angrysamaritan.webtestsystem.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatNotificationsConfig implements Serializable {

    @ManyToOne
    @JoinColumn(name = "chat_id")
    @Id
    private Chat chat;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @Id
    private User user;

    boolean enabled;
}

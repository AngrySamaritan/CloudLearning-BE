package com.angrysamaritan.webtestsystem.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "role_id")
    @EqualsAndHashCode.Exclude
    private int role_id;

    @Enumerated(EnumType.STRING)
    @Column(name = "role_name")
    private Names roleName;

    @ManyToMany(mappedBy = "roles")
    @EqualsAndHashCode.Exclude
    private List<User> users;

    public Role(Names roleName) {
        this.role_id = roleName.getCode();
        this.roleName = roleName;
    }

    @Getter
    public enum Names {
        ROLE_UNCONFIRMED(-1, "UNCONFIRMED"),
        ROLE_USER(0, "USER"),
        ROLE_MODER(1, "MODER"),
        ROLE_ADMIN(2, "ADMIN");

        private final int code;

        private final String shortName;

        Names(int code, String shortName) {
            this.code = code;
            this.shortName = shortName;
        }

    }
}

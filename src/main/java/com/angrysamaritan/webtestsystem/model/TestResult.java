package com.angrysamaritan.webtestsystem.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class TestResult {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long resultId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private Test test;

    private Date completionDate;

    private Time completionTime;

    private Time completionDuration;

    private long points;

    @OneToMany(mappedBy = "testResult")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private List<QuestionResult> questionResults;
}

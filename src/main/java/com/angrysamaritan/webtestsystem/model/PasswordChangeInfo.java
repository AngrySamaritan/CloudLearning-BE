package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@EqualsAndHashCode
@Getter
@Setter
public class PasswordChangeInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String confirmationCode;

    private Date invalidateDate;

    private Time invalidateTime;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}

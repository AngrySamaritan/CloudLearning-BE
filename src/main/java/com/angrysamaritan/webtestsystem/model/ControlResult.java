package com.angrysamaritan.webtestsystem.model;

public enum  ControlResult {
    WRONG_CHECKED,
    RIGHT_CHECKED,
    WRONG_UNCHECKED,
    RIGHT_UNCHECKED
}

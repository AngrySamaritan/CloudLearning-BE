package com.angrysamaritan.webtestsystem.model;

public enum QuestionType {
    SINGLE_SELECT_QUESTION,
    MULTIPLE_SELECT_QUESTION,
    WRITING_QUESTION
}

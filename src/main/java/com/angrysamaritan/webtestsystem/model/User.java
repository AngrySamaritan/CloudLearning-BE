package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@Getter
@Setter
@EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long userId;

    @Field(index = Index.YES)
    private String username;

    private String password;

    @Field(index = Index.YES)
    private String firstName;

    @Field(index = Index.YES)
    private String lastName;

    private String email;

    @EqualsAndHashCode.Exclude
    private Long totalPoints;

    @ManyToMany(mappedBy = "allowedUsers")
    @EqualsAndHashCode.Exclude
    private List<Test> allowedTests;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    @EqualsAndHashCode.Exclude
    private List<Role> roles;

    @ManyToOne
    @JoinColumn(name = "profile_image_id_")
    private Image profileImage;

    @OneToMany(mappedBy = "user")
    @EqualsAndHashCode.Exclude
    private List<TestResult> testResults;

    @OneToMany(mappedBy = "author")
    @EqualsAndHashCode.Exclude
    private List<Test> createdTests;

    @ManyToOne
    @JoinColumn(name = "group_id")
    @Cascade(org.hibernate.annotations.CascadeType.PERSIST)
    private Group group;

    @ManyToMany
    @JoinTable(name = "moders_of_subjects",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "subject_id")})
    @EqualsAndHashCode.Exclude
    private List<Subject> assignedSubjects;

    @OneToOne(mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private ConfirmationInfo confirmationInfo;

    @ManyToMany
    @JoinTable(name = "user_chat",
            inverseJoinColumns = {@JoinColumn(name = "chat_id")},
            joinColumns = {@JoinColumn(name = "user_id")})
    private List<Chat> chats;

    @OneToMany(mappedBy = "recipient")
    private List<Notification> notifications;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<ChatNotificationsConfig> chatNotificationsConfigs;

    @OneToMany(mappedBy = "user")
    private List<NewMessagesInChat> newMessagesInfo;
}

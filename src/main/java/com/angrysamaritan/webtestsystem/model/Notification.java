package com.angrysamaritan.webtestsystem.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Notification {
    @Id
    @Type(type = "uuid-char")
    @GeneratedValue(generator = "uuid2")
    private UUID id;

    @ElementCollection
    @MapKeyColumn(name = "param")
    @Column(name = "value")
    @CollectionTable(name = "notifications_params", joinColumns = @JoinColumn(name = "id"))
    private Map<String, String> params;

    @JsonIgnore
    private NotificationStatus status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User recipient;

    @Enumerated(EnumType.STRING)
    private NotificationTypes type;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTimestamp;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTimestamp;

    public enum NotificationStatus {
        DELIVERED,
        READY_TO_DELIVERY
    }

    public enum NotificationTypes {
        ACCESS_TO_TEST_GRANTED,
        SUBJECTS_ASSIGNEE_CHANGED,
        NEW_MESSAGES,
    }

}

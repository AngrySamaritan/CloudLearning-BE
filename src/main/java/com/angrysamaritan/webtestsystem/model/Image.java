package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class Image {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id_")
    @EqualsAndHashCode.Exclude
    private String id;

    @Lob
    private byte[] bytes;

    @EqualsAndHashCode.Exclude
    private String name;

    @OneToMany(mappedBy = "profileImage", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private List<User> profiles;

    @EqualsAndHashCode.Exclude
    private int hashcode;
}

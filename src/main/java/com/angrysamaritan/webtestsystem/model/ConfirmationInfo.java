package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@EqualsAndHashCode
public class ConfirmationInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private long id;

    @OneToOne
    @Getter
    @Setter
    private User user;

    @Getter
    @Setter
    private String confirmationCode;

    @Getter
    @Setter
    private Date deleteDate;

    @Getter
    @Setter
    private Time deleteTime;
}

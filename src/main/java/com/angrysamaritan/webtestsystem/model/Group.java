package com.angrysamaritan.webtestsystem.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.List;

@Entity
@Indexed
@Table(name = "group_")
@EqualsAndHashCode
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private int id;

    @Field(index = Index.YES)
    @Getter
    private String code;

    @OneToMany(mappedBy = "group")
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private List<User> users;

    public Group setId(int id) {
        this.id = id;
        return this;
    }

    public Group setCode(String code) {
        this.code = code;
        return this;
    }
}

package com.angrysamaritan.webtestsystem.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Constraint(validatedBy = AnswersAmountValidator.class)
public @interface ValidAnswersAmount {
    int maxWritingAnswers() default 10;
    int minSelectAnswers() default 2;
    int maxSelectAnswers() default 8;
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

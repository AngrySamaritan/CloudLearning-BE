package com.angrysamaritan.webtestsystem.validators;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.google.common.primitives.Ints.min;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface StrongPassword {
    String message() default "Invalid password: password must be from 8 to 16 symbols length," +
            " contains at least 1 uppercase letter(s) and at least 1 digit(s)";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    int minLength() default 8;
    int maxLength() default 16;
    int digitsAmount() default 1;
    int upperCaseAmount() default 1;
}

package com.angrysamaritan.webtestsystem.validators;

import com.angrysamaritan.webtestsystem.exceptions.BadRequestException;
import com.angrysamaritan.webtestsystem.model.Question;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AnswersAmountValidator implements ConstraintValidator<ValidAnswersAmount, Question> {

    private int maxWritingAnswers;
    private int maxSelectAnswers;
    private int minSelectAnswers;

    @Override
    public void initialize(ValidAnswersAmount constraintAnnotation) {
        maxWritingAnswers = constraintAnnotation.maxWritingAnswers();
        if (maxWritingAnswers < 1) throw new IllegalArgumentException("Max answers amount can't be less than 1");
        minSelectAnswers = constraintAnnotation.minSelectAnswers();
        maxSelectAnswers = constraintAnnotation.maxSelectAnswers();
        if (minSelectAnswers < 2) throw new IllegalArgumentException(
                "Min select answers amount can't be less than 2");
        if (maxSelectAnswers < minSelectAnswers) throw
                new IllegalArgumentException("Max select answers amount can't be less than min");
    }

    @Override
    public boolean isValid(Question question, ConstraintValidatorContext constraintValidatorContext) {
        switch (question.getQuestionType()) {
            case WRITING_QUESTION:
                return question.getAnswers().size() > 0 || question.getAnswers().size() <= maxWritingAnswers;
            case SINGLE_SELECT_QUESTION:
            case MULTIPLE_SELECT_QUESTION:
                return question.getAnswers().size() >= minSelectAnswers &&
                        question.getAnswers().size() <= maxSelectAnswers;

            default:
                throw new BadRequestException("Wrong question type format");
        }
    }
}


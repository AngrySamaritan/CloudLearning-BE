package com.angrysamaritan.webtestsystem.validators;

import com.angrysamaritan.webtestsystem.dto.user.IPasswordWithConfirmation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, IPasswordWithConfirmation> {
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {

    }

    @Override
    public boolean isValid(IPasswordWithConfirmation userDto, ConstraintValidatorContext constraintValidatorContext) {
        return userDto.getPassword().equals(userDto.getPasswordConfirmation());
    }
}

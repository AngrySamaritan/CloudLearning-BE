package com.angrysamaritan.webtestsystem.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Constraint(validatedBy = HasRightAnswersValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface HasRightAnswers {
    String message() default "Question must have at least one right answer";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

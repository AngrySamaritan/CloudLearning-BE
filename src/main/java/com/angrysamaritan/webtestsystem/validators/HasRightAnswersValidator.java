package com.angrysamaritan.webtestsystem.validators;

import com.angrysamaritan.webtestsystem.dto.FullAnswerDto;
import com.angrysamaritan.webtestsystem.dto.FullQuestionDto;
import com.angrysamaritan.webtestsystem.model.QuestionType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HasRightAnswersValidator implements ConstraintValidator<HasRightAnswers, FullQuestionDto> {

    @Override
    public void initialize(HasRightAnswers constraintAnnotation) {

    }

    @Override
    public boolean isValid(FullQuestionDto questionDtoReq, ConstraintValidatorContext constraintValidatorContext) {
        if (questionDtoReq.getQuestionType() == QuestionType.WRITING_QUESTION) return true;
        return questionDtoReq.getAnswers().stream().anyMatch(FullAnswerDto::isRight);
    }
}

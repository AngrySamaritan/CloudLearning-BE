package com.angrysamaritan.webtestsystem.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<StrongPassword, String> {

    private int maxLength;
    private int minLength;
    private int upperCase;
    private int digits;
    private String message;

    @Override
    public void initialize(StrongPassword constraintAnnotation) {
        minLength = constraintAnnotation.minLength();
        maxLength = constraintAnnotation.maxLength();
        if (minLength >= maxLength) throw new IllegalArgumentException("The minimum password length can not be equals or" +
                " greater than the maximum");
        upperCase = constraintAnnotation.upperCaseAmount();
        if (digits >= maxLength) throw new IllegalArgumentException("The upper case letters amount can not be equals or" +
                " greater than the maximum");
        digits = constraintAnnotation.digitsAmount();
        if (upperCase >= maxLength) throw new IllegalArgumentException("The digits amount can not be equals or" +
                " greater than the maximum");
        message = String.format("Invalid password: password must be from %d to %d symbols length," +
                " contains at least %d uppercase letter(s) and at least %d digit(s)", minLength, maxLength, upperCase, digits);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        PasswordValidationInfo passwordValidationInfo = new PasswordValidationInfo(s);
        boolean valid = s.length() >= minLength && s.length() <= maxLength && passwordValidationInfo.getUpperCount() >= upperCase
                && passwordValidationInfo.getNumberCount() >= digits;
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        return valid;
    }

    static class PasswordValidationInfo {

        private int upperCount = 0;

        private int numberCount = 0;

        PasswordValidationInfo(String password) {
            for (int i = 0; i < password.length(); i++) {
                char c = password.charAt(i);
                if (Character.isDigit(c)) {
                    numberCount++;
                }
                if (Character.isUpperCase(c)) {
                    upperCount++;
                }
            }
        }

        public int getUpperCount() {
            return upperCount;
        }

        public void setUpperCount(int upperCount) {
            this.upperCount = upperCount;
        }

        public int getNumberCount() {
            return numberCount;
        }

        public void setNumberCount(int numberCount) {
            this.numberCount = numberCount;
        }
    }

}

package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Chat;
import com.angrysamaritan.webtestsystem.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ChatRepository extends CrudRepository<Chat, String> {

    @Query("SELECT m FROM Chat c JOIN Message m ON m.chat = c WHERE c.id = ?1 ORDER BY m.timestamp DESC")
    Page<Message> getMessagesFromChat(String chatId, Pageable pageable);

    @Query("SELECT nm.amount FROM NewMessagesInChat nm WHERE nm.user.userId = ?2 AND nm.chat.id = ?1")
    Integer getNewMessagesAmount(String chatId, long userId);

    @Query("SELECT cfg.enabled FROM ChatNotificationsConfig cfg WHERE cfg.user.userId = ?2 AND cfg.chat.id = ?1")
    Boolean isNotificationEnabled(String chatId, long userId);

    @Modifying
    @Query("UPDATE NewMessagesInChat nm SET nm.amount = ?3 WHERE nm.chat.id = ?1 and nm.user.userId = ?2")
    @Transactional
    void setNewMessagesCount(String chatId, long userId, long amount);

    @Transactional
    Chat findByHashcode(int hash);
}

package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.ConfirmationInfo;
import com.angrysamaritan.webtestsystem.model.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ConfirmationInfoRepository extends CrudRepository<ConfirmationInfo, Long> {

    @Query("SELECT u.userId FROM User u WHERE u.username = ?1")
    ConfirmationInfo getUserByUsername(String username);

    @Query("SELECT u FROM ConfirmationInfo u WHERE u.confirmationCode = ?1")
    ConfirmationInfo getUserByConfirmationCode(String confirmationCode);

    @Transactional
    @Modifying
    @Query("SELECT u FROM ConfirmationInfo c JOIN c.user u WHERE c.deleteDate < current_date OR" +
            " (c.deleteDate = current_date AND c.deleteTime <= current_time)")
    List<User> selectOverdue();
}

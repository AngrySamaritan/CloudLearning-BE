package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.CorsInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorsInfoRepository extends CrudRepository<CorsInfo, Integer> {

    @Query("SELECT c FROM CorsInfo c WHERE c.hostName = ?1")
    CorsInfo getCorsInfoByHostName(String hostName);
}

package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GroupRepository extends CrudRepository<Group, Integer> {

    @Query("SELECT  g FROM Group g WHERE g.code = ?1")
    Group getGroupByCode(String code);
}

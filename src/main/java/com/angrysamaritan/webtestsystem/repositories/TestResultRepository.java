package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.TestResult;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestResultRepository extends CrudRepository<TestResult, Long> {
}

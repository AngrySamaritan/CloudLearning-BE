package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.TestCompletingInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Repository
public interface TestCompletingInfoRepository extends CrudRepository<TestCompletingInfo, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE FROM TestCompletingInfo t WHERE t.invalidateTime <= current_time ")
    void clearOverdue();


    @Query("SELECT t FROM TestCompletingInfo t WHERE t.user.userId = ?2 AND t.test.testId = ?1")
    TestCompletingInfo getInfo(long testId, long userId);

    List<TestCompletingInfo> getAllByUser_UserId_In(Collection<Long> user_userId);
}

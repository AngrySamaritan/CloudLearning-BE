package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Test;
import com.angrysamaritan.webtestsystem.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TestRepository extends CrudRepository<Test, Long> {

    @Query("SELECT u.userId FROM Test t JOIN t.allowedUsers u WHERE t.testId = ?1 AND u.userId = ?2")
    Object[] checkIfAllowed(Long testId, Long userId);

    @Query("SELECT t FROM Test t JOIN t.allowedUsers u WHERE t.subject.subjectId = ?1 AND u.userId = ?2")
    Test[] getAllowed(int subjectId, long userId);

    @Query("SELECT t FROM Subject s JOIN s.tests t WHERE s.subjectId = ?1")
    List<Test> getAll(int subjectId);

    @Query("SELECT t.allowedUsers FROM Test t WHERE t.testId = :testId")
    List<User> getAllowedUsers(long testId);
}

package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Transactional
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u.userId FROM User u WHERE u.email = ?1")
    List<Long> getIdByEmail(String email);

    User getUserByUsername(String username);

    @Query("SELECT u FROM User  u WHERE u.username = ?1 OR u.email = ?1")
    User getUserByUsernameOrEmail(String emailOrUsername);

    List<User> getAllByUserIdIn(Collection<Long> userId);
}

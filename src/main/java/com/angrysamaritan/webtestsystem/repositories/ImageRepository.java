package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Image;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends CrudRepository<Image, String> {

    List<Image> getAllByHashcode(int hashcode);

}

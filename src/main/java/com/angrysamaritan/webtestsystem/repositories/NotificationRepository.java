package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, UUID> {

    Page<Notification> findAllByRecipient_UserId_OrderByUpdateTimestampDesc(long recipient_userId, Pageable pageable);
}

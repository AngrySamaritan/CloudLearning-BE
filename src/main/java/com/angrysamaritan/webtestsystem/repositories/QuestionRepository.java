package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.Question;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {
}

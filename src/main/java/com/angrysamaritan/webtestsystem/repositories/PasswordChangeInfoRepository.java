package com.angrysamaritan.webtestsystem.repositories;

import com.angrysamaritan.webtestsystem.model.PasswordChangeInfo;
import com.angrysamaritan.webtestsystem.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordChangeInfoRepository extends CrudRepository<PasswordChangeInfo, Long> {

    @Query("SELECT u.username FROM PasswordChangeInfo p JOIN p.user u WHERE p.confirmationCode = ?1")
    String checkCode(String code);

    @Query("SELECT u FROM PasswordChangeInfo p JOIN p.user u WHERE p.confirmationCode = ?1")
    User getUser(String code);

    @Query("SELECT p FROM PasswordChangeInfo p WHERE p.confirmationCode = ?1")
    PasswordChangeInfo getInfo(String code);
}

package com.angrysamaritan.webtestsystem.config;

import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.services.implementations.UserDetailsServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

import static com.angrysamaritan.webtestsystem.config.KeyWords.SUBJECTS;
import static com.angrysamaritan.webtestsystem.config.KeyWords.USERS;

@Configuration
@EnableWebSecurity
@Log4j2
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String ADMIN_SHORT_NAME = Role.Names.ROLE_ADMIN.getShortName();
    public final String[] SWAGGER_URLS = {
            "/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            "/api/**"
    };

    private final UserDetailsServiceImpl userDetailsService;

    private final DataSource dataSource;

    @Autowired
    public SecurityConfig(UserDetailsServiceImpl userDetailsService, DataSource dataSource,
                          JWTFilter jwtFilter, CorsFilter corsFilter) {
        this.userDetailsService = userDetailsService;
        this.dataSource = dataSource;
        this.jwtFilter = jwtFilter;
        this.corsFilter = corsFilter;
    }

    @Autowired
    BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private final JWTFilter jwtFilter;

    private final CorsFilter corsFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http
                .authorizeRequests()
                .antMatchers("/sign_up", "/confirm/*", "/password/**", "/log_out").permitAll()
                .antMatchers("/confirmed", "/test/**", "/main", "/login")
                .access("hasAnyRole('ROLE_USER', 'ROLE_MODER', 'ROLE_ADMIN')")
                .antMatchers(SWAGGER_URLS).permitAll()
                .antMatchers(HttpMethod.PATCH, SUBJECTS + "/**").hasRole(ADMIN_SHORT_NAME)
                .antMatchers(HttpMethod.DELETE, SUBJECTS).hasRole(ADMIN_SHORT_NAME)
                .antMatchers(HttpMethod.PUT, SUBJECTS).hasRole(ADMIN_SHORT_NAME)
                .antMatchers(HttpMethod.PATCH, USERS + "/*" + SUBJECTS).hasRole(ADMIN_SHORT_NAME)
                .anyRequest().authenticated()
                .and().httpBasic()
                .authenticationEntryPoint(noWWWBasicAuthenticationPoint())
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable();

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(corsFilter, JWTFilter.class);

    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public NoWWWBasicAuthenticationPoint noWWWBasicAuthenticationPoint() {
        return new NoWWWBasicAuthenticationPoint();
    }

    private static class NoWWWBasicAuthenticationPoint implements AuthenticationEntryPoint {

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response,
                             AuthenticationException e) throws IOException {
            response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
        }
    }
}

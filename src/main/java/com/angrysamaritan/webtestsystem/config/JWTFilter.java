package com.angrysamaritan.webtestsystem.config;

import com.angrysamaritan.webtestsystem.services.implementations.JwtTokenServiceImpl;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

import static com.angrysamaritan.webtestsystem.config.KeyWords.TOKEN;

@Component
@Log4j2
public class JWTFilter extends OncePerRequestFilter {

    final
    UserDetailsService userDetailsService;

    final
    JwtTokenServiceImpl jwtTokenService;

    public JWTFilter(UserDetailsService userDetailsServiceImpl, JwtTokenServiceImpl jwtTokenService) {
        this.userDetailsService = userDetailsServiceImpl;
        this.jwtTokenService = jwtTokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            if (httpServletRequest.getCookies() != null) {
                final String jwtToken = Arrays.stream(httpServletRequest.getCookies())
                        .filter(cookie -> cookie.getName().equals(TOKEN)).map(Cookie::getValue).findFirst().orElse(null);

                if (jwtToken != null) {
                    String username = null;

                    try {

                        username = jwtTokenService.getUsernameFromToken(jwtToken);

                    } catch (IllegalArgumentException e) {

                        log.error("Unable to get JWT Token");

                    } catch (ExpiredJwtException e) {

                        log.info("JWT Token has expired");

                    }

                    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                        if (jwtTokenService.validateToken(jwtToken, userDetails)) {
                            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                    new UsernamePasswordAuthenticationToken(
                                            userDetails, null, userDetails.getAuthorities());
                            usernamePasswordAuthenticationToken.setDetails(
                                    new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                        }

                    }
                }
            }
        } finally {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}

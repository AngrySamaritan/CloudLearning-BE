package com.angrysamaritan.webtestsystem.config;

import com.angrysamaritan.webtestsystem.model.CorsInfo;
import com.angrysamaritan.webtestsystem.repositories.CorsInfoRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CorsFilter extends OncePerRequestFilter {

    private final CorsInfoRepository corsInfoRepository;

    public CorsFilter(CorsInfoRepository corsInfoRepository) {
        this.corsInfoRepository = corsInfoRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String url = httpServletRequest.getHeader("origin");CorsInfo corsInfo = corsInfoRepository.getCorsInfoByHostName(url);
        if (corsInfo != null && corsInfo.isAllowed()) {
            httpServletResponse.addHeader("Access-Control-Allow-Origin", corsInfo.getHostName());
            httpServletResponse.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PATCH, PUT, DELETE");
            httpServletResponse.addHeader("Access-Control-Allow-Headers", "*");
            httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
            if (!httpServletRequest.getMethod().equals("OPTIONS")) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
        } else {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }
}

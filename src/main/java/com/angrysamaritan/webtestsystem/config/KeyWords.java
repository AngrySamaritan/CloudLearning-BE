package com.angrysamaritan.webtestsystem.config;

public class KeyWords {

    // URLS

    public static final String TESTS = "/tests";
    public static final String CHECK = "/check";
    public static final String BRIEF = "/brief";

    public static final String MANAGEMENT = "/management";
    public static final String USERS = "/users";
    public static final String ME = "/me";
    public static final String ACCESS = "/access";

    public static final String GROUPS = "/groups";

    public static final String RESULTS = "/results";

    public static final String IMAGES = "/images";

    public static final String SUBJECTS = "/subjects";

    public static final String FULL = "/full";

    public static final String SEARCH = "/search";

    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/log_out";

    public static final String CHATS = "/chats";

    // DB

    public static final String QUESTION = "question";

    // COOKIES

    public static final String TOKEN = "token";

}

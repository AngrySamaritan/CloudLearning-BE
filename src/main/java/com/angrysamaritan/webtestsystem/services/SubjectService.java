package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.SubjectInfoResp;

import java.util.List;

public interface SubjectService {
    SubjectInfoResp getSubjectInfo(int subjectId);

    List<SubjectInfoResp> getAllSubjects();

    boolean isModer(int subjectId, long userId);

    void deleteSubject(int id);

    void renameSubject(int id, String name);

    void createSubject(String name);

    void patchModers(int id, List<Long> userIds);
}

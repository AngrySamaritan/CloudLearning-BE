package com.angrysamaritan.webtestsystem.services;

import javax.mail.MessagingException;

public interface MailService {

    void sendMail(String usernameOrEmail, String subject, String htmlMessage) throws MessagingException;

    void sendMail(long userId, String subject, String htmlMessage) throws MessagingException;

}

package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.notifications.NotificationDto;
import com.angrysamaritan.webtestsystem.model.Chat;

import java.util.List;
import java.util.UUID;

public interface NotificationService {

    void sendAccessToTestGrantedNotification(long recipientId, long testId, long moderId);

    void sendSubjectAssigneeChangedNotification(long recipientId, long adminId);

    void newMessagesNotification(long recipientId, Chat chatId);

    List<NotificationDto> getAllNotifications(int page, long userId);

    void markAsDelivered(UUID id, long userId);

    void deleteNotification(UUID id);
}

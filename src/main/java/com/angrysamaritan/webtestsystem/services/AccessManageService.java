package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.NoAccessToTestException;

import java.util.List;

public interface AccessManageService {

    void checkAccess(long testId, long userId) throws NoAccessToTestException;

    void hasAccessToManageTest(long testId, long userId) throws NoAccessToTestException;

    void testCheckingAccess(long testId, long userId);

    void patchUsersAccess(long testId, List<Long> accessedUsersIds);

    void invalidateAccess(long test, long authorizedUser);

    void startSession(long userId, long testId);

    List<UserInfoResp> getAllowedUsers(long testId);
}
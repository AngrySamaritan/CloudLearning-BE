package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.dto.user.ProfileEditDto;

public interface ProfileService {
    void editProfile(ProfileEditDto newData, long userId);

    ProfileDtoResp getProfile(long userId);
}

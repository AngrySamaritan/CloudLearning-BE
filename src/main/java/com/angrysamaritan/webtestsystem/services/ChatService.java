package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.ChatDto;
import com.angrysamaritan.webtestsystem.dto.MessageDto;
import com.angrysamaritan.webtestsystem.model.Chat;

import java.util.List;

public interface ChatService {

    List<ChatDto> getChats(long userId);

    String createChat(ChatDto chatDto, long userId);

    List<MessageDto> getMessages(String chatId, long userId, int page);

    boolean hasNewMessages(long userId);

    void sendMessage(MessageDto messageDto, Chat chatId, long senderId);
}

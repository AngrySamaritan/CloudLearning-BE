package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.FullTestDto;

public interface TestCreateService {
    void saveOrUpdateTest(FullTestDto testDto, long authorId);
}

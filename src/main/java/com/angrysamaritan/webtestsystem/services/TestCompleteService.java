package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.exceptions.QuestionNotFoundException;

import java.util.Map;

public interface TestCompleteService {
    long checkTest(long testId, Map<Long, Object> answers, long userId) throws QuestionNotFoundException;
}

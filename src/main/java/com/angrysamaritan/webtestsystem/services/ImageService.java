package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.model.Image;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    String upload(MultipartFile image) throws IOException;

    Image download(String id);
}

package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.notifications.NotificationDto;
import com.angrysamaritan.webtestsystem.exceptions.NotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Chat;
import com.angrysamaritan.webtestsystem.model.Notification;
import com.angrysamaritan.webtestsystem.model.Test;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.NotificationRepository;
import com.angrysamaritan.webtestsystem.repositories.TestRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.NotificationService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class NotificationServiceImpl implements NotificationService {

    @Value("${app.notifications.max-notifications-on-page}")
    private int MAX_NOTIFICATIONS_ON_PAGE;

    private final UserRepository userRepository;
    private final NotificationRepository notificationRepository;
    private final SimpMessagingTemplate messagingTemplate;
    private final TestRepository testRepository;

    public NotificationServiceImpl(UserRepository userRepository, NotificationRepository notificationRepository, SimpMessagingTemplate messagingTemplate, TestRepository testRepository) {
        this.userRepository = userRepository;
        this.notificationRepository = notificationRepository;
        this.messagingTemplate = messagingTemplate;
        this.testRepository = testRepository;
    }

    private void sendNotification(User recipient, Map<String, String> params, Notification.NotificationTypes type) {
        try {
            Notification notification = new Notification();
            if (params.containsKey("prevId")) {
                notification.setId(UUID.fromString(params.get("prevId")));
            }
            notification.setRecipient(recipient);
            notification.setStatus(Notification.NotificationStatus.READY_TO_DELIVERY);
            notification.setParams(params);
            notification.setType(type);
            notification = notificationRepository.save(notification);
            messagingTemplate.convertAndSendToUser(recipient.getUserId() + "", "/notifications",
                    new NotificationDto(notification));
            notificationRepository.save(notification);
        } catch (Exception e) {
            log.error("Cant send notification to user id: {} caused by: ", recipient.getUserId(), e);
        }
    }

    @Override
    public void sendAccessToTestGrantedNotification(long recipientId, long testId, long moderId) {
        User recipient = userRepository.findById(recipientId).orElseThrow(() -> new UserNotFoundException(recipientId));
        User moder = userRepository.findById(moderId).orElseThrow(() -> new UserNotFoundException(moderId));
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        sendNotification(recipient, new HashMap<>() {{
            put("testId", testId + "");
            put("testName", test.getName());
            put("moderId", moderId + "");
            put("moderName", moder.getFirstName() + " " + moder.getLastName());
        }}, Notification.NotificationTypes.ACCESS_TO_TEST_GRANTED);
    }

    @Override
    public void sendSubjectAssigneeChangedNotification(long recipientId, long adminId) {
        User recipient = userRepository.findById(recipientId).orElseThrow(() -> new UserNotFoundException(recipientId));
        User admin = userRepository.findById(adminId).orElseThrow(() -> new UserNotFoundException(adminId));
        sendNotification(recipient, new HashMap<>() {{
            put("adminName", admin.getFirstName() + " " + admin.getLastName());
            put("userId", recipient.getUserId() + "");
        }}, Notification.NotificationTypes.SUBJECTS_ASSIGNEE_CHANGED);
    }

    @Override
    public void newMessagesNotification(long recipientId, Chat chat) {
        User recipient = userRepository.findById(recipientId).orElseThrow(() -> new UserNotFoundException(recipientId));
        String prevId = notificationRepository.
                findAllByRecipient_UserId_OrderByUpdateTimestampDesc(recipientId, PageRequest.of(0, 1))
                .stream().map(notification -> notification.getId().toString()).findFirst().orElse(null);
        sendNotification(recipient, new HashMap<>() {{
            put("userId", recipient.getUserId() + "");
            if (prevId != null) {
                put("prevId", prevId);
            }
            put("chatName", Optional.ofNullable(chat.getName()).orElse(chat.getParticipants()
                    .stream().filter(user -> !(user.getUserId() == recipientId))
                    .map(u -> u.getFirstName() + " " + u.getLastName()).findFirst().orElse("No Name")));
        }}, Notification.NotificationTypes.NEW_MESSAGES);
    }

    @Override
    @Transactional
    public List<NotificationDto> getAllNotifications(int page, long userId) {
        PageRequest pageable = PageRequest.of(page, MAX_NOTIFICATIONS_ON_PAGE);
        Page<Notification> notifications = notificationRepository
                .findAllByRecipient_UserId_OrderByUpdateTimestampDesc(userId, pageable);

        List<NotificationDto> notificationDtoList = notifications
                .stream().map(NotificationDto::new)
                .collect(Collectors.toList());

        notificationRepository.saveAll(notifications.stream()
                .filter(notification -> notification.getStatus().equals(Notification.NotificationStatus.READY_TO_DELIVERY))
                .peek(notification -> notification.setStatus(Notification.NotificationStatus.DELIVERED))
                .collect(Collectors.toList()));
        return notificationDtoList;
    }

    @Override
    public void markAsDelivered(UUID id, long userId) {
        Notification notification = notificationRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Notification id = " + id + " not found"));
        if (notification.getRecipient().getUserId() == userId) {
            notification.setStatus(Notification.NotificationStatus.DELIVERED);
            notificationRepository.save(notification);
        } else {
            throw new NotFoundException("Notification id = " + id + " not found");
        }
    }

    @Override
    public void deleteNotification(UUID id) {
        notificationRepository.deleteById(id);
    }
}
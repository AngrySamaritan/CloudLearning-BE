package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.ChatDto;
import com.angrysamaritan.webtestsystem.dto.MessageDto;
import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.exceptions.AccessException;
import com.angrysamaritan.webtestsystem.exceptions.BadRequestException;
import com.angrysamaritan.webtestsystem.exceptions.ChatNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.*;
import com.angrysamaritan.webtestsystem.repositories.ChatRepository;
import com.angrysamaritan.webtestsystem.repositories.ImageRepository;
import com.angrysamaritan.webtestsystem.repositories.MessagesRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.ChatService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final MessagesRepository messagesRepository;

    @Value("${app.messages.messages-per-request}")
    private int MESSAGES_PER_REQUEST;

    public ChatServiceImpl(ChatRepository chatRepository, UserRepository userRepository, ImageRepository imageRepository, SimpMessagingTemplate simpMessagingTemplate, MessagesRepository messagesRepository) {
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.messagesRepository = messagesRepository;
    }

    @Override
    public List<ChatDto> getChats(long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        return user.getChats().stream().map(chat -> new ChatDto(
                chat.getId(),
                Optional.ofNullable(chat.getName()).orElse(chat.getParticipants().stream()
                        .filter(p -> p.getUserId() != userId)
                        .map(u -> u.getFirstName() + " " + u.getLastName())
                        .findFirst().orElse("No Name")),
                Optional.ofNullable(chat.getImageId()).orElse(chat.getParticipants().stream()
                        .filter(p -> p.getUserId() != userId)
                        .map(User::getProfileImage)
                        .filter(Objects::nonNull)
                        .map(Image::getId)
                        .findFirst().orElse(null)),
                chat.getParticipants().stream().map(ProfileDtoResp::new).collect(Collectors.toList()),
                null,
                chatRepository.getNewMessagesAmount(chat.getId(), userId),
                chatRepository.isNotificationEnabled(chat.getId(), userId)
        )).collect(Collectors.toList());
    }

    @Override
    public String createChat(ChatDto chatDto, long userId) {
        if (!chatDto.getParticipantsIds().contains(userId)) {
            throw new BadRequestException("Can't create chat without participating it");
        }
        if (chatDto.getParticipantsIds().stream().noneMatch(id -> id != userId)) {
            throw new BadRequestException("Can't create chat with yourself");
        }
        Chat chat = new Chat();
        chat.setImageId(chatDto.getImageId());
        chat.setName(chatDto.getName());
        chat.setParticipants(new LinkedList<>());
        chat.setNewMessagesInfo(new LinkedList<>());
        chat.setChatNotificationsConfigs(new LinkedList<>());
        userRepository.findAllById(chatDto.getParticipantsIds()).forEach(u -> {
            chat.getParticipants().add(u);
            chat.getNewMessagesInfo().add(new NewMessagesInChat(chat, u, 0));
            chat.getChatNotificationsConfigs().add(new ChatNotificationsConfig(chat, u, true));
        });
        Chat c1;
        if ((c1 = chatRepository.findByHashcode(chat.hashCode())) != null) {
            return c1.getId();
        }
        chat.setHashcode(chat.hashCode());
        return chatRepository.save(chat).getId();
    }

    @Override
    @Transactional
    public List<MessageDto> getMessages(String chatId, long userId, int page) {
        Chat chat = chatRepository.findById(chatId).orElseThrow(() -> new ChatNotFoundException(chatId));
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        if (chat.getParticipants().contains(user)) {
            List<Message> messages = chatRepository
                    .getMessagesFromChat(chatId, PageRequest.of(page, MESSAGES_PER_REQUEST)).toList();
            List<MessageDto> messageDtos = messages.stream().map(m -> new MessageDto(m, user))
                    .collect(Collectors.toList());
            messagesRepository.saveAll(messages.stream().peek(message -> message.getUsersReadied().add(user))
                    .collect(Collectors.toList()));
            chatRepository.setNewMessagesCount(chatId, userId, 0L);
            messageDtos.sort(Comparator.comparing(MessageDto::getTimestamp));
            return messageDtos;
        } else {
            throw new AccessException("No access to this chat");
        }
    }

    @Override
    public boolean hasNewMessages(long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId))
                .getNewMessagesInfo()
                .stream().anyMatch(i -> i.getAmount() > 0);
    }

    @Override
    public void sendMessage(MessageDto messageDto, Chat chat, long senderId) {
        User user = userRepository.findById(senderId).orElseThrow(() -> new UserNotFoundException(senderId));
        Message message = new Message();
        message.setChat(chat);
        message.setText(messageDto.getText());
        message.setAttachments(new LinkedList<>());
        imageRepository.findAllById(messageDto.getAttachments()).forEach(a -> message.getAttachments().add(a));
        message.setSender(userRepository.findById(senderId).orElseThrow(() -> new UserNotFoundException(senderId)));
        chat.getMessages().add(message);
        messageDto.setSender(new ProfileDtoResp(user));
        simpMessagingTemplate.convertAndSend("/messages/" + messageDto.getChatId(), messageDto);
        chat.getNewMessagesInfo().stream().filter(i -> i.getUser().getUserId() != senderId)
                .forEach(i -> i.setAmount(i.getAmount() + 1));
        chatRepository.save(chat);
    }
}

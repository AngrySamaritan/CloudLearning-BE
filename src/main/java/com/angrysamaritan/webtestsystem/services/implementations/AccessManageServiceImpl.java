package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.NoAccessToTestException;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.model.Test;
import com.angrysamaritan.webtestsystem.model.TestCompletingInfo;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.TestCompletingInfoRepository;
import com.angrysamaritan.webtestsystem.repositories.TestRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.AccessManageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccessManageServiceImpl implements AccessManageService {

    private final TestRepository testRepository;

    private final UserRepository userRepository;

    public final long AIRBAG_MINUTES = 2;

    private final TestCompletingInfoRepository testCompletingInfoRepository;

    public AccessManageServiceImpl(TestRepository testRepository, UserRepository userRepository, TestCompletingInfoRepository testCompletingInfoRepository) {
        this.testRepository = testRepository;
        this.userRepository = userRepository;
        this.testCompletingInfoRepository = testCompletingInfoRepository;
    }

    @Override
    public void checkAccess(long testId, long userId) throws NoAccessToTestException {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        if (!(testRepository.checkIfAllowed(testId, userId).length != 0
                || test.getSubject().getTeachers().contains(user)
                || user.getRoles().contains(new Role(Role.Names.ROLE_ADMIN))))
            throw new NoAccessToTestException(testId);
    }

    @Override
    public void hasAccessToManageTest(long testId, long userId) throws NoAccessToTestException {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        isAuthor(test, user);
    }

    private boolean isAuthor(Test test, User user) {
        return test.getAuthor().getUserId() == (user.getUserId());
    }

    public void patchUsersAccess(long testId, List<Long> accessedUsersIds) {
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        List<Long> completingNowIds = testCompletingInfoRepository.getAllByUser_UserId_In(accessedUsersIds).stream()
                .map(TestCompletingInfo::getUser)
                .map(User::getUserId)
                .collect(Collectors.toList());
        List<User> accessedUsers = userRepository.getAllByUserIdIn(accessedUsersIds).stream()
                .filter(user -> !completingNowIds.contains(user.getUserId()))
                .collect(Collectors.toList());
        test.setAllowedUsers(accessedUsers);
        testRepository.save(test);
    }

    public void invalidateAccess(long testId, long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        test.getAllowedUsers().remove(user);
    }

    @Override
    public void startSession(long testId, long userId) {
        TestCompletingInfo testCompletingInfo = new TestCompletingInfo();
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        testCompletingInfo.setTest(test);
        testCompletingInfo.setUser(user);
        Time invalidatingTime = Time.valueOf(LocalTime.now().plusMinutes(test.getDuration() + AIRBAG_MINUTES));
        testCompletingInfo.setInvalidateTime(invalidatingTime);
        testCompletingInfoRepository.save(testCompletingInfo);
    }

    @Override
    public List<UserInfoResp> getAllowedUsers(long testId) {
        List<User> allowedUsers = testRepository.getAllowedUsers(testId);
        List<UserInfoResp> allowedUsersDto = new LinkedList<>();
        for (User user : allowedUsers) {
            allowedUsersDto.add(new UserInfoResp(user));
        }
        return allowedUsersDto;
    }

    @Transactional
    public void testCheckingAccess(long testId, long userId) {
        TestCompletingInfo info = testCompletingInfoRepository.getInfo(testId, userId);
        if (info == null) {
            throw new NoAccessToTestException(testId);
        } else {
            invalidateTestCompletingInfo(info);
        }
    }

    public void invalidateTestCompletingInfo(TestCompletingInfo info) {
        testCompletingInfoRepository.deleteById(info.getId());
    }


}

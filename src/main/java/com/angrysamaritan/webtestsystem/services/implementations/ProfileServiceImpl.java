package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.dto.user.ProfileEditDto;
import com.angrysamaritan.webtestsystem.exceptions.NotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Group;
import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.GroupRepository;
import com.angrysamaritan.webtestsystem.repositories.ImageRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;

    private final ImageRepository imageRepository;

    private final GroupRepository groupRepository;

    @Autowired
    public ProfileServiceImpl(UserRepository userRepository, ImageRepository imageRepository, GroupRepository groupRepository) {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    public void editProfile(ProfileEditDto newData, long userId) {
        var user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        if (newData.getFirstName() != null) {
            user.setFirstName(newData.getFirstName());
        }
        if (newData.getLastName() != null) {
            user.setLastName(newData.getLastName());
        }
        if (newData.getGroup() != null) {
            String groupCode = newData.getGroup();
            Group group = groupRepository.getGroupByCode(groupCode);
            if (group == null) {
                group = new Group();
                group.setCode(groupCode);
                List<User> userList = new ArrayList<>();
                userList.add(user);
                group.setUsers(userList);
            }
            user.setGroup(group);
        }

        if (newData.getImageId() != null && !newData.getImageId().isBlank()) {
            Image image = imageRepository.findById(newData.getImageId())
                    .orElseThrow(() -> new NotFoundException("Image " + newData.getImageId() + " not found!"));

            user.setProfileImage(image);
        }

        userRepository.save(user);
    }

    @Override
    public ProfileDtoResp getProfile(long userId) {
        var user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        return new ProfileDtoResp(user);
    }
}

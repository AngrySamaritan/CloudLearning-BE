package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.results.TestResultForOwner;
import com.angrysamaritan.webtestsystem.dto.results.TestResultDto;
import com.angrysamaritan.webtestsystem.dto.results.TestResultInfo;
import com.angrysamaritan.webtestsystem.exceptions.ResultNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.model.TestResult;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.TestResultRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.TestResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;


@Service
public class TestResultServiceImpl implements TestResultService {

    private final TestResultRepository testResultRepository;

    private final UserRepository userRepository;

    @Autowired
    public TestResultServiceImpl(TestResultRepository testResultRepository, UserRepository userRepository) {
        this.testResultRepository = testResultRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public TestResultDto getTestResult(long id, long userId) throws Exception {
        var authorizedUser = userRepository.findById(userId).orElseThrow(() -> new Exception("Authorized user not found"));
        TestResult testResult = getTestResult(id);
        if (authorizedUser.getRoles().contains(new Role(Role.Names.ROLE_ADMIN)) || isAuthor(authorizedUser, testResult)
                || authorizedUser.getUserId() == (getTestResult(id).getUser().getUserId())) {
            return new TestResultForOwner(testResult);
        }
        return new TestResultDto(testResult);
    }


    private boolean isAuthor(User user, TestResult testResult) {
        return user.equals(testResult.getUser());
    }


    private TestResult getTestResult(Long id) throws ResultNotFoundException {
        return testResultRepository.findById(id).orElseThrow(() -> new ResultNotFoundException(id));

    }

    @Override
    public List<TestResultInfo> getResultList(long userId) {
        List<TestResultInfo> resultList = new LinkedList<>();
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        for (TestResult result : user.getTestResults()) {
            resultList.add(new TestResultInfo(result));

        }
        return resultList;
    }
}

package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailServiceImpl implements MailService {

    private final JavaMailSender mailSender;

    private final UserRepository userRepository;

    @Value("${app.mail.sender}")
    private String sender;


    @Autowired
    public MailServiceImpl(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") JavaMailSender mailSender,
                           UserRepository userRepository) {
        this.mailSender = mailSender;
        this.userRepository = userRepository;
    }


    @Override
    public void sendMail(String usernameOrEmail, String subject, String htmlMessage) throws MessagingException {
        var user = userRepository.getUserByUsernameOrEmail(usernameOrEmail);
        if (user == null) {
            throw new UserNotFoundException(usernameOrEmail);
        }
        sendMain(subject, htmlMessage, user);
    }

    private void sendMain(String subject, String htmlMessage, User user) throws MessagingException {
        MimeMessage msg = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true, "utf-8");
        msg.setFrom(sender);
        helper.setTo(user.getEmail());
        helper.setSubject(subject);
        msg.setContent(htmlMessage, "text/html");
        mailSender.send(msg);
    }

    @Override
    public void sendMail(long userId, String subject, String htmlMessage) throws MessagingException {
        var user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        sendMain(subject, htmlMessage, user);
    }
}

package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.exceptions.NotFoundException;
import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.repositories.ImageRepository;
import com.angrysamaritan.webtestsystem.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public String upload(MultipartFile image) throws IOException {
        Image img = new Image();
        img.setBytes(image.getBytes());
        img.setName(image.getName());
        img.setHashcode(img.hashCode());
        List<Image> l = imageRepository.getAllByHashcode(img.getHashcode()).stream()
                .filter(i -> Arrays.equals(img.getBytes(), i.getBytes())).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(l)) {
            return l.get(0).getId();
        }
        return imageRepository.save(img).getId();
    }

    @Override
    public Image download(String id) {
        return imageRepository.findById(id).orElseThrow(() -> new NotFoundException("Image " + id + " not found"));
    }
}

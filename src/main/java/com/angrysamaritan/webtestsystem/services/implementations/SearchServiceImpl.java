package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.model.Group;
import com.angrysamaritan.webtestsystem.model.Test;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.services.SearchService;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class SearchServiceImpl implements SearchService {

    @Value("${app.search.max-results}")
    private final int MAX_RESULTS = 10;

    private final EntityManager entityManager;

    @Autowired
    public SearchServiceImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<ProfileDtoResp> searchUser(String searchQuery) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(User.class)
                .get();

        Query query = queryBuilder.keyword().wildcard()
                .onField("firstName")
                .andField("lastName")
                .andField("username")
                .matching("**" + searchQuery.toLowerCase() + "**")
                .createQuery();

        FullTextQuery jpaQuery = fullTextEntityManager.createFullTextQuery(query, User.class).setMaxResults(MAX_RESULTS);

        @SuppressWarnings("unchecked")
        List<User> searchResult = jpaQuery.getResultList();

        return searchResult.stream().map(ProfileDtoResp::new).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<GroupInfoResp> searchGroup(String searchQuery) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Group.class)
                .get();

        Query query = queryBuilder
                .keyword()
                .wildcard()
                .onField("code")
                .matching("**" + searchQuery + "**")
                .createQuery();

        FullTextQuery jpaQuery = fullTextEntityManager.createFullTextQuery(query, Group.class);

        @SuppressWarnings("unchecked")
        List<Group> searchResult = jpaQuery.getResultList();
        List<GroupInfoResp> groupInfoList = new LinkedList<>();

        for(Group group : searchResult) {
            groupInfoList.add(new GroupInfoResp(group));
        }

        return groupInfoList;
    }

    @Override
    public List<TestInfoResp> searchTest(String searchQuery) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder queryBuilder = fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Test.class)
                .get();

        Query query = queryBuilder.keyword().wildcard()
                .onField("name").matching("**" + searchQuery + "**").createQuery();

        FullTextQuery jpaQuery = fullTextEntityManager.createFullTextQuery(query, Test.class);
        @SuppressWarnings("unchecked")
        List<Test> searchResult = jpaQuery.getResultList();

        List<TestInfoResp> testInfoList = new LinkedList<>();

        for(Test test : searchResult) {
            testInfoList.add(new TestInfoResp(test));
        }

        return testInfoList;
    }
}

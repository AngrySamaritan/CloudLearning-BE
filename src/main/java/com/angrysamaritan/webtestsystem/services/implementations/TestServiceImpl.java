package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.FullTestDto;
import com.angrysamaritan.webtestsystem.dto.test.TestDtoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.model.Test;
import com.angrysamaritan.webtestsystem.repositories.TestRepository;
import com.angrysamaritan.webtestsystem.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestServiceImpl implements TestService {

    private final TestRepository testRepository;

    @Autowired
    public TestServiceImpl(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    @Override
    public List<TestInfoResp> getAllTests(int subjectId, long userId, boolean asAdmin, boolean asModer) {
        return testRepository.getAll(subjectId).stream()
                .filter(test -> test.getAllowedUsers().stream().anyMatch(user -> user.getUserId() == userId)
                        || asAdmin
                        || asModer)
                .filter(test -> !test.isMarkedForDelete())
                .map(test -> new TestInfoResp(test, asAdmin || test.getAuthor().getUserId() == userId))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public TestDtoResp getTest(long testId) {
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        if (test.isMarkedForDelete()) {
            throw new TestNotFoundException(testId);
        }
        return new TestDtoResp(test);
    }

    @Override
    public TestInfoResp getTestInfo(long testId) {
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        if (test.isMarkedForDelete()) {
            throw new TestNotFoundException(testId);
        }
        return new TestInfoResp(test);
    }

    @Override
    public void markForDelete(long id) {
        Test test = testRepository.findById(id).orElseThrow(() -> new TestNotFoundException(id));
        test.setMarkedForDelete(true);
        test.getAllowedUsers().clear();
        testRepository.save(test);
    }

    @Override
    public FullTestDto getTestForEditing(long testId) {
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        return new FullTestDto(test);
    }

    @Override
    public boolean isAuthor(long testId, long userId) {
        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        return test.getAuthor().getUserId() == userId;
    }
}

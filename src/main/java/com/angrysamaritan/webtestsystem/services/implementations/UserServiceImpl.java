package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.user.SignUpReq;
import com.angrysamaritan.webtestsystem.exceptions.CodeNotExistException;
import com.angrysamaritan.webtestsystem.exceptions.NotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.*;
import com.angrysamaritan.webtestsystem.repositories.ConfirmationInfoRepository;
import com.angrysamaritan.webtestsystem.repositories.RoleRepository;
import com.angrysamaritan.webtestsystem.repositories.SubjectRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final int HOURS_TO_CONFIRM = 24;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    private final UserRepository userRepository;

    private final ConfirmationInfoRepository confirmationInfoRepository;

    private final RoleRepository roleRepository;

    private final SubjectRepository subjectRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, ConfirmationInfoRepository confirmationInfoRepository, SubjectRepository subjectRepository) {
        this.userRepository = userRepository;
        this.confirmationInfoRepository = confirmationInfoRepository;
        this.roleRepository = roleRepository;
        this.subjectRepository = subjectRepository;
    }

    public List<GrantedAuthority> getGrantedAuthorities(User user) {

        List<GrantedAuthority> authorityList = new ArrayList<>();
        for (Role role : user.getRoles()) {
            authorityList.add(new SimpleGrantedAuthority(role.getRoleName().name()));
        }
        return authorityList;
    }

    @Override
    public long registerNewAccount(SignUpReq userDto, String confirmationCode, Group group)
            throws Exception {
        User user = new User();

        user.setUsername(userDto.getUsername());

        user.setFirstName(userDto.getFirstName());

        user.setLastName(userDto.getLastName());

        user.setPassword(encoder.encode(userDto.getPassword()));

        ConfirmationInfo confirmationInfo = new ConfirmationInfo();

        confirmationInfo.setUser(user);

        confirmationInfo.setConfirmationCode(confirmationCode);

        LocalDateTime deletingDateTime = LocalDateTime.now().plusHours(HOURS_TO_CONFIRM);

        confirmationInfo.setDeleteDate(Date.valueOf(deletingDateTime.toLocalDate()));

        confirmationInfo.setDeleteTime(Time.valueOf(deletingDateTime.toLocalTime()));

        user.setConfirmationInfo(confirmationInfo);

        user.setEmail(userDto.getEmail());

        user.setTotalPoints((long) 0);

        List<Role> defaultRoles = new ArrayList<>();

        defaultRoles.add(roleRepository.findById(Role.Names.ROLE_UNCONFIRMED.getCode())
                .orElseThrow(() -> new Exception("Role not found")));

        user.setRoles(defaultRoles);

        user.setGroup(group);

        return userRepository.save(user).getUserId();
    }

    public void activateUserByCode(String code) throws Exception {
        try {
            ConfirmationInfo confirmationInfo = confirmationInfoRepository.getUserByConfirmationCode(code);
            if (confirmationInfo == null) {
                throw new CodeNotExistException("Code" + code + "not exist");
            }
            User user = confirmationInfo.getUser();
            List<Role> defaultRoles = new ArrayList<>();
            defaultRoles.add(roleRepository.findById(Role.Names.ROLE_USER.getCode()).orElseThrow(() -> new Exception("Role not found")));
            user.setRoles(defaultRoles);
            userRepository.save(user);
            confirmationInfoRepository.delete(confirmationInfo);
        } catch (IndexOutOfBoundsException e) {
            throw new CodeNotExistException("Confirmation code " + code + " not exist.");
        }
    }


    public boolean hasRole(long userId, Role.Names role) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        return user.getRoles().contains(new Role(role));
    }

    public String getHtmlLetter(String url) {
        return "<h1>Cloud learning</h1>" +
                "\n<h3>To confirm your email follow current link: " +
                "<a href=\"" +
                url +
                "\">" +
                "confirm." +
                "</a></h3>";
    }

    @Override
    public long getUserIdByName(String username) {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new UserNotFoundException(username);
        }
        return user.getUserId();
    }

    @Override
    @Transactional
    public void assigneeSubjects(long userId, List<Integer> subjectsId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        user.getAssignedSubjects().clear();
        Iterable<Subject> subjects = subjectRepository.findAllById(subjectsId);
        subjects.forEach(subject -> user.getAssignedSubjects().add(subject));
        Role moderRole = roleRepository.findById(Role.Names.ROLE_MODER.getCode()).orElseThrow(
                () -> new NotFoundException("Role not found"));
        if (!user.getAssignedSubjects().isEmpty() && !hasRole(userId, Role.Names.ROLE_MODER)) {
            user.getRoles().add(moderRole);
        } else {
            if (user.getAssignedSubjects().isEmpty() && hasRole(userId, Role.Names.ROLE_MODER)) {
                user.getRoles().remove(moderRole);
            }
        }
        userRepository.save(user);
    }

    @Override
    public void checkRoles(Set<Long> usersIds) {

        Role moderRole = roleRepository.findById(Role.Names.ROLE_MODER.getCode()).orElseThrow();

        List<User> users = new LinkedList<>();
        userRepository.findAllById(usersIds).forEach(users::add);
        users = users.stream().filter(u ->
                u.getAssignedSubjects().size() == 0 && u.getRoles().contains(new Role(Role.Names.ROLE_MODER))
                        || u.getAssignedSubjects().size() != 0 && !u.getRoles().contains(new Role(Role.Names.ROLE_MODER)))
                .collect(Collectors.toList());
        users.forEach(u -> {
            if (u.getAssignedSubjects().size() == 0 && u.getRoles().contains(new Role(Role.Names.ROLE_MODER))) {
                u.getRoles().remove(moderRole);
            } else {
                u.getRoles().add(moderRole);
            }
        });
        userRepository.saveAll(users);
    }
}

package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.dto.group.GroupUsersResp;
import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.GroupNotFoundException;
import com.angrysamaritan.webtestsystem.model.Group;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.GroupRepository;
import com.angrysamaritan.webtestsystem.services.GroupService;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public GroupUsersResp getUsersList(int id) {
        var group = getGroup(id);
        var users = new LinkedList<UserInfoResp>();
        for (User user: group.getUsers()) {
            users.add(new UserInfoResp(user));
        }
        return new GroupUsersResp(group.getId(), group.getCode(), users);
    }

    public Group getGroup(int groupId) throws GroupNotFoundException {
        return groupRepository.findById(groupId).orElseThrow(() -> new GroupNotFoundException(groupId));
    }

    public List<GroupInfoResp> getGroups() {
        var groups = new LinkedList<GroupInfoResp>();
        for (Group group : groupRepository.findAll()) {
            groups.add(new GroupInfoResp(group.getId(), group.getCode()));
        }
        return groups;
    }

    public Group getGroupByCode(String groupCode) {
        if (groupCode == null || groupCode.equals("")) {
            return null;
        }
        Group group = groupRepository.getGroupByCode(groupCode);
        if (group == null) {
            group = new Group().setCode(groupCode);
            group = groupRepository.save(group);
        }
        return group;
    }
}

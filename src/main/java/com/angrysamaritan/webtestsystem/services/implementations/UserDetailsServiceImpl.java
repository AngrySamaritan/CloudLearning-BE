package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    final UserRepository userRepository;
    final UserServiceImpl userService;

    public UserDetailsServiceImpl(UserRepository userRepository, UserServiceImpl userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user;
        List<GrantedAuthority> authorityList;

        user = userRepository.getUserByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException("User " + s + " not found");
        }

        authorityList = userService.getGrantedAuthorities(user);

        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), authorityList);
    }


}

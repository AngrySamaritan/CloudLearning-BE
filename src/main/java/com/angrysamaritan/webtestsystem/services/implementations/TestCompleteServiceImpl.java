package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.exceptions.QuestionNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.*;
import com.angrysamaritan.webtestsystem.repositories.QuestionRepository;
import com.angrysamaritan.webtestsystem.repositories.TestRepository;
import com.angrysamaritan.webtestsystem.repositories.TestResultRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.TestCompleteService;
import com.angrysamaritan.webtestsystem.util.PointCountStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
@Service
public class TestCompleteServiceImpl implements TestCompleteService {

    private final TestResultRepository testResultRepository;

    private final TestRepository testRepository;

    private final QuestionRepository questionRepository;

    private final UserRepository userRepository;

    private final PointCountStrategy pointCountStrategy;

    @Autowired
    public TestCompleteServiceImpl(TestResultRepository testResultRepository, TestRepository testRepository,
                                   QuestionRepository questionRepository, UserRepository userRepository,
                                   PointCountStrategy defaultPointCountStrategy) {
        this.testResultRepository = testResultRepository;
        this.testRepository = testRepository;
        this.questionRepository = questionRepository;
        this.userRepository = userRepository;
        this.pointCountStrategy = defaultPointCountStrategy;
    }

    @Override
    public long checkTest(long testId, Map<Long, Object> answers, long userId) throws QuestionNotFoundException {

        Test test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        TestResult testResult = new TestResult();

        testResult.setTest(test);
        testResult.setPoints(0);

        List<QuestionResult> questionResults = new LinkedList<>();

        for (Map.Entry<Long, Object> entry : answers.entrySet()) {
            long questionId = entry.getKey();
            Question question = questionRepository.findById(questionId).orElseThrow(
                    () -> new QuestionNotFoundException(questionId));
            QuestionResult result = new QuestionResult();
            int points = 0;
            switch (question.getQuestionType()) {
                case WRITING_QUESTION:
                    result = processWritingQuestion((String) answers.get(questionId), question);
                    points = pointCountStrategy.countPointsWriting(question, result);
                    break;
                case SINGLE_SELECT_QUESTION:
                    Object obj = answers.get(questionId);
                    result = processSingleSelectQuestion(obj != null ? String.valueOf(obj) : "-1", question);
                    points = pointCountStrategy.countPointsSingleSelect(question, result);
                    break;
                case MULTIPLE_SELECT_QUESTION:
                    result = processMultipleSelectQuestion((List<String>) answers.get(questionId), question);
                    points = pointCountStrategy.countPointsMultipleSelect(question, result);
                    break;
            }
            result.setPointsAmount(points);
            testResult.setPoints(testResult.getPoints() + points);
            result.setTestResult(testResult);
            questionResults.add(result);
        }

        testResult.setQuestionResults(questionResults);
        testResult.setUser(user);
        testResult.setCompletionDate(Date.valueOf(LocalDate.now()));
        testResult.setCompletionTime(Time.valueOf(LocalTime.now()));

        testResult = testResultRepository.save(testResult);

        return testResult.getResultId();
    }

    private QuestionResult processSingleSelectQuestion(String answerId, Question question) {
        QuestionResult questionResult = new QuestionResult();
        questionResult.setQuestion(question);
        questionResult.setAnswers(new LinkedList<>());
        for (Answer answer : question.getAnswers()) {
            AnswerResult answerResult = new AnswerResult();
            answerResult.setAnswer(answer);
            answerResult.setQuestionResult(questionResult);
            if (answer.isRight()) {
                if (answer.getAnswerId().equals(answerId)) {
                    answerResult.setControlResult(ControlResult.RIGHT_CHECKED);
                } else {
                    answerResult.setControlResult(ControlResult.WRONG_UNCHECKED);
                }
            } else {
                if (answer.getAnswerId().equals(answerId)) {
                    answerResult.setControlResult(ControlResult.WRONG_CHECKED);
                } else {
                    answerResult.setControlResult(ControlResult.RIGHT_UNCHECKED);
                }
            }
            questionResult.getAnswers().add(answerResult);
        }
        return questionResult;
    }

    private QuestionResult processMultipleSelectQuestion(List<String> answerIds, Question question) {
        QuestionResult questionResult = new QuestionResult();
        questionResult.setQuestion(question);
        questionResult.setAnswers(new LinkedList<>());
        for (Answer answer : question.getAnswers()) {
            AnswerResult answerResult = new AnswerResult();
            answerResult.setAnswer(answer);
            answerResult.setQuestionResult(questionResult);
            if (answer.isRight()) {
                if (answerIds != null && answerIds.contains(answer.getAnswerId())) {
                    answerResult.setControlResult(ControlResult.RIGHT_CHECKED);
                } else {
                    answerResult.setControlResult(ControlResult.WRONG_UNCHECKED);
                }
            } else {
                if (answerIds != null && answerIds.contains(answer.getAnswerId())) {
                    answerResult.setControlResult(ControlResult.WRONG_CHECKED);
                } else {
                    answerResult.setControlResult(ControlResult.RIGHT_UNCHECKED);
                }
            }
            questionResult.getAnswers().add(answerResult);
        }
        return questionResult;
    }

    private QuestionResult processWritingQuestion(String answer, Question question) {
        QuestionResult questionResult = new QuestionResult();
        questionResult.setAnswers(new LinkedList<>());
        questionResult.setQuestion(question);
        if (question.getAnswers().stream().anyMatch(expected -> expected.getAnswerText().toLowerCase(Locale.ROOT)
                .equals(answer.toLowerCase(Locale.ROOT)))) {
            questionResult.setPointsAmount(question.getPointsAmount());
            questionResult.getAnswers().add(new AnswerResult() {{
                setControlResult(ControlResult.RIGHT_CHECKED);
                setQuestionResult(questionResult);
                setAnswer(question.getAnswers().stream().filter(
                        a -> a.getAnswerText().toLowerCase(Locale.ROOT).equals(answer.toLowerCase(Locale.ROOT))).
                        collect(Collectors.toList()).get(0));
            }});
        } else {
            questionResult.setPointsAmount(0);
            questionResult.getAnswers().addAll(question.getAnswers().stream().map(a -> new AnswerResult() {{
                setAnswer(a);
                setQuestionResult(questionResult);
                setControlResult(ControlResult.WRONG_UNCHECKED);
            }}).collect(Collectors.toList()));
        }

        return questionResult;
    }

}

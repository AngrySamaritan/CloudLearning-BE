package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.dto.SubjectInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.BadRequestException;
import com.angrysamaritan.webtestsystem.exceptions.SubjectNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Subject;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.SubjectRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;

    private final UserRepository userRepository;

    @Autowired
    public SubjectServiceImpl(SubjectRepository subjectRepository, UserRepository userRepository) {
        this.subjectRepository = subjectRepository;
        this.userRepository = userRepository;
    }

    @Override
    public SubjectInfoResp getSubjectInfo(int subjectId) {
        Subject subject = subjectRepository.findById(subjectId).filter(s -> !s.isMarkedForDelete())
                .orElseThrow(() -> new SubjectNotFoundException(subjectId));
        return new SubjectInfoResp(subject);
    }

    @Override
    public List<SubjectInfoResp> getAllSubjects() {
        List<SubjectInfoResp> subjects = new LinkedList<>();
        subjectRepository.findAll().forEach(
                (subject) -> {
                    if (!subject.isMarkedForDelete()) {
                        subjects.add(new SubjectInfoResp(subject));
                    }
                }
        );
        return subjects;
    }

    @Override
    @Transactional
    public boolean isModer(int subjectId, long userId) {
        Subject subject = subjectRepository.findById(subjectId).filter(s -> !s.isMarkedForDelete())
                .orElseThrow(() -> new SubjectNotFoundException(subjectId));
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
        return subject.getTeachers().contains(user);
    }

    @Override
    public void deleteSubject(int id) {
        Subject subject = subjectRepository.findById(id).filter(s -> !s.isMarkedForDelete())
                .orElseThrow(() -> new SubjectNotFoundException(id));
        subject.setMarkedForDelete(true);
        subjectRepository.save(subject);
    }

    @Override
    public void renameSubject(int id, String name) {
        if (name == null || name.equals("")) {
            throw new BadRequestException("Name can't be blank");
        }
        Subject subject = subjectRepository.findById(id).filter(s -> !s.isMarkedForDelete())
                .orElseThrow(() -> new SubjectNotFoundException(id));
        subject.setName(name);
        subjectRepository.save(subject);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void createSubject(String name) {
        if (name == null || name.equals("")) {
            throw new BadRequestException("Name can't be blank");
        }
        if (subjectRepository.findByNameAndMarkedForDeleteIsFalse(name).isEmpty()) {
            Subject subject = new Subject();
            subject.setName(name);
            subjectRepository.save(subject);
        } else {
            throw new BadRequestException("Subject name already exist");
        }
    }

    @Override
    public void patchModers(int id, List<Long> userIds) {
        List<User> users = userRepository.getAllByUserIdIn(userIds);
        Subject subject = getSubject(id);
        subject.setTeachers(users);
        subjectRepository.save(subject);
    }

    private Subject getSubject(int id) {
        return subjectRepository.findById(id).orElseThrow(() -> new SubjectNotFoundException(id));
    }
}

package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.repositories.TestCompletingInfoRepository;
import com.angrysamaritan.webtestsystem.repositories.ConfirmationInfoRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService {

    public static final int CHECK_OVERDUE_TEST_COMPLETING_DELAY_SECONDS = 10;
    private static final int CHECK_OVERDUE_USERS_DELAY_HOURS = 1;

    private final TestCompletingInfoRepository testCompletingInfoRepository;
    
    private final ConfirmationInfoRepository confirmationInfoRepository;

    private final UserRepository userRepository;

    public ScheduleService(TestCompletingInfoRepository testCompletingInfoRepository, ConfirmationInfoRepository confirmationInfoRepository, UserRepository userRepository) {
        this.testCompletingInfoRepository = testCompletingInfoRepository;
        this.confirmationInfoRepository = confirmationInfoRepository;
        this.userRepository = userRepository;
    }

    @Scheduled(fixedDelay = CHECK_OVERDUE_TEST_COMPLETING_DELAY_SECONDS * 1000)
    public void clearOverdueTestInfo() {
        testCompletingInfoRepository.clearOverdue();
    }

    @Scheduled(fixedDelay = CHECK_OVERDUE_USERS_DELAY_HOURS * 60 * 60 * 1000)
    public void clearUnconfirmed() {
        userRepository.deleteAll(confirmationInfoRepository.selectOverdue());
    }
}

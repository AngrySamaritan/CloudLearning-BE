package com.angrysamaritan.webtestsystem.services.implementations;

import com.angrysamaritan.webtestsystem.exceptions.CodeNotExistException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.PasswordChangeInfo;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.PasswordChangeInfoRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.PasswordChangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class PasswordChangeServiceImpl implements PasswordChangeService {

    public static final int HOURS_TO_CHANGE_PASSWORD = 2;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    private final UserRepository userRepository;

    private final PasswordChangeInfoRepository passwordChangeInfoRepository;

    @Autowired
    public PasswordChangeServiceImpl(UserRepository userRepository, PasswordChangeInfoRepository passwordChangeInfoRepository) {
        this.userRepository = userRepository;
        this.passwordChangeInfoRepository = passwordChangeInfoRepository;
    }

    @Override
    public String savePasswordChangeInfo(String usernameOrEmail) {

        String code = UUID.randomUUID().toString();
        User user = userRepository.getUserByUsernameOrEmail(usernameOrEmail);
        if (user == null) {
            throw new UserNotFoundException(usernameOrEmail);
        }
        PasswordChangeInfo passwordChangeInfo = new PasswordChangeInfo();
        passwordChangeInfo.setConfirmationCode(code);
        passwordChangeInfo.setUser(user);
        LocalDateTime invalidateDateTime = LocalDateTime.now().plusHours(HOURS_TO_CHANGE_PASSWORD);
        passwordChangeInfo.setInvalidateDate(Date.valueOf(invalidateDateTime.toLocalDate()));
        passwordChangeInfo.setInvalidateTime(Time.valueOf(invalidateDateTime.toLocalTime()));
        passwordChangeInfoRepository.save(passwordChangeInfo);
        return code;
    }

    @Override
    public void changePassword(String code, String password) {
        User user = passwordChangeInfoRepository.getUser(code);
        if (user == null) {
            throw new CodeNotExistException(code);
        }
        passwordChangeInfoRepository.delete(passwordChangeInfoRepository.getInfo(code));
        password = encoder.encode(password);
        user.setPassword(password);
        userRepository.save(user);
    }

    @Override
    public String getHtmlLetter(String url) {
        return "<h1>Cloud learning</h1>" +
                "\n<h3>To change your password follow current link: " +
                "<a href=\"" +
                url +
                "\">" +
                "change password." +
                "</a></h3>";
    }

}

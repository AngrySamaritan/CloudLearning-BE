package com.angrysamaritan.webtestsystem.services.implementations;


import com.angrysamaritan.webtestsystem.dto.FullAnswerDto;
import com.angrysamaritan.webtestsystem.dto.FullQuestionDto;
import com.angrysamaritan.webtestsystem.dto.FullTestDto;
import com.angrysamaritan.webtestsystem.exceptions.NotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.SubjectNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.*;
import com.angrysamaritan.webtestsystem.repositories.*;
import com.angrysamaritan.webtestsystem.services.TestCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestCreateServiceImpl implements TestCreateService {

    private final TestRepository testRepository;
    private final UserRepository userRepository;
    private final SubjectRepository subjectRepository;
    private final QuestionRepository questionRepository;
    private final ImageRepository imageRepository;


    @Autowired
    public TestCreateServiceImpl(TestRepository testRepository, UserRepository userRepository, SubjectRepository subjectRepository, QuestionRepository questionRepository, ImageRepository imageRepository) {
        this.testRepository = testRepository;
        this.userRepository = userRepository;
        this.subjectRepository = subjectRepository;
        this.questionRepository = questionRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    @Transactional
    public void saveOrUpdateTest(FullTestDto testDto, long authorId) {
        Long testId = testDto.getTestId();
        Test test;
        if (testId != null) {
            test = testRepository.findById(testId).orElseThrow(() -> new TestNotFoundException(testId));
            test.getQuestions().forEach(question -> {
                question.setTest(null);
                questionRepository.save(question);
            });
            test.getQuestions().clear();
        } else {
            test = new Test();
        }
        Subject subject = subjectRepository.findById(testDto.getSubjectId())
                .orElseThrow(() -> new SubjectNotFoundException(testDto.getSubjectId()));
        test.setSubject(subject);

        if (test.getAuthor() == null) {
            User author = userRepository.findById(authorId).orElseThrow(() -> new UserNotFoundException(authorId));
            test.setAuthor(author);
        }

        test.setDuration(testDto.getDuration());

        List<Question> questionList = testDto.getQuestions().stream().map(this::mapQuestion)
                .peek(question -> question.setTest(test)).collect(Collectors.toList());

        test.setMaxPoints(questionList.stream().map(Question::getPointsAmount).reduce(0, Integer::sum));
        test.setQuestions(questionList);
        test.setName(testDto.getName());
        test.setTestDescription(testDto.getTestDescription());
        testRepository.save(test);
    }

    private Question mapQuestion(FullQuestionDto questionDto) {
        Question question = Question.builder()
                .pointsAmount(questionDto.getPointsAmount())
                .questionText(questionDto.getQuestionText())
                .questionType(questionDto.getQuestionType())
                .build();
        if (questionDto.getImageId() != null) {
            question.setImage(imageRepository.findById(questionDto.getImageId())
                    .orElseThrow(() -> new NotFoundException("Image not found")));
        }
        List<Answer> answers = questionDto.getAnswers().stream().map(this::mapAnswer)
                .peek(answer -> answer.setQuestion(question)).collect(Collectors.toList());
        question.setAnswers(answers);
        return question;
    }

    private Answer mapAnswer(FullAnswerDto answerDto) {
        return Answer.builder()
                .answerText(answerDto.getAnswerText())
                .isRight(answerDto.isRight())
                .build();
    }
}

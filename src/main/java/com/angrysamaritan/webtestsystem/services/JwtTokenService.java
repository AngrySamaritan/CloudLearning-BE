package com.angrysamaritan.webtestsystem.services;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtTokenService {

    String generateToken(UserDetails userDetails);

}

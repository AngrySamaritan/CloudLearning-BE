package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.user.SignUpReq;
import com.angrysamaritan.webtestsystem.model.Group;
import com.angrysamaritan.webtestsystem.model.Role;

import java.util.List;
import java.util.Set;

public interface UserService {

    long registerNewAccount(SignUpReq userDto, String confirmationCode, Group group) throws Exception;

    void activateUserByCode(String code) throws Exception;

    String getHtmlLetter(String url);

    long getUserIdByName(String username);

    boolean hasRole(long id, Role.Names role);

    void assigneeSubjects(long userId, List<Integer> subjectsId);

    void checkRoles(Set<Long> usersIds);
}

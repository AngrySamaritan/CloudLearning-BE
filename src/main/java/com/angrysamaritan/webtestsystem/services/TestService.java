package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.FullTestDto;
import com.angrysamaritan.webtestsystem.dto.test.TestDtoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;

import java.util.List;

public interface TestService {

    List<TestInfoResp> getAllTests(int subjectId, long userId, boolean asAdmin, boolean asModer);

    TestDtoResp getTest(long testId);

    TestInfoResp getTestInfo(long testId);

    void markForDelete(long id);

    FullTestDto getTestForEditing(long testId);

    boolean isAuthor(long testId, long userId);
}

package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.dto.group.GroupUsersResp;

import java.util.List;

public interface GroupService {

    List<GroupInfoResp> getGroups();

    GroupUsersResp getUsersList(int id);

}

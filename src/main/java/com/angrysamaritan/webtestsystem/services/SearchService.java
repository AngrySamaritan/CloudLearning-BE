package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;

import java.util.List;

public interface SearchService {
    List<ProfileDtoResp> searchUser(String searchQuery);

    List<GroupInfoResp> searchGroup(String searchQuery);

    List<TestInfoResp> searchTest(String searchQuery);
}

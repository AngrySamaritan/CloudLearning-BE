package com.angrysamaritan.webtestsystem.services;

import com.angrysamaritan.webtestsystem.dto.results.TestResultDto;
import com.angrysamaritan.webtestsystem.dto.results.TestResultInfo;

import javax.transaction.Transactional;
import java.util.List;

public interface TestResultService {
    @Transactional
    TestResultDto getTestResult(long id, long userId) throws Exception;

    List<TestResultInfo> getResultList(long userId);
}

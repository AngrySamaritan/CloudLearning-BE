package com.angrysamaritan.webtestsystem.services;

public interface PasswordChangeService {

    String savePasswordChangeInfo(String usernameOrEmail);

    void changePassword(String code, String password);

    String getHtmlLetter(String url);

}

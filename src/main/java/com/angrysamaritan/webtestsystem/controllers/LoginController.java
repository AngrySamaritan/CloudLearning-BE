package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.implementations.JwtTokenServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserDetailsServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import com.angrysamaritan.webtestsystem.services.JwtTokenService;
import com.angrysamaritan.webtestsystem.services.UserService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import static com.angrysamaritan.webtestsystem.config.KeyWords.*;

@RestController
@Api
@Log4j2
public class LoginController {

    final UserRepository userRepository;

    final JwtTokenService jwtTokenService;

    final UserDetailsService userDetailsService;

    final UserService userService;

    public LoginController(UserRepository userRepository, JwtTokenServiceImpl jwtTokenService, UserDetailsServiceImpl userDetailsService, UserServiceImpl userService) {
        this.userRepository = userRepository;
        this.jwtTokenService = jwtTokenService;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
    }


    @PostMapping(LOGIN)
    public void login(HttpServletResponse response) {

        String token = jwtTokenService.generateToken(userDetailsService.loadUserByUsername(
                SecurityContextHolder.getContext().getAuthentication().getName()));
        Cookie tokenCookie = new Cookie(TOKEN, token);
        tokenCookie.setHttpOnly(true);
        response.addCookie(tokenCookie);
    }

    @PostMapping(LOGOUT)
    public void logout(HttpServletResponse response) {
        Cookie tokenCookie = new Cookie(TOKEN, null);
        tokenCookie.setHttpOnly(true);
        tokenCookie.setMaxAge(0);
        response.addCookie(tokenCookie);
    }
}

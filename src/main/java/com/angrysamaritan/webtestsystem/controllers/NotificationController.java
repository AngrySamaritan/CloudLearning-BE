package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.notifications.NotificationDto;
import com.angrysamaritan.webtestsystem.services.NotificationService;
import com.angrysamaritan.webtestsystem.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/notifications")
public class NotificationController {

    private final NotificationService notificationService;
    private final UserService userService;

    public NotificationController(NotificationService notificationService, UserService userService) {
        this.notificationService = notificationService;
        this.userService = userService;
    }

    @GetMapping
    public List<NotificationDto> getAllNotifications(@RequestParam(name = "page", defaultValue = "0") int page,
                                                     Principal principal) {
        return notificationService.getAllNotifications(page, userService.getUserIdByName(principal.getName()));
    }

    @PatchMapping
    public void markAsReadied(@RequestParam(name = "id") UUID id, Principal principal) {
        notificationService.markAsDelivered(id, userService.getUserIdByName(principal.getName()));
    }

    @DeleteMapping
    public void deleteNotification(@RequestParam(name = "id") UUID id) {
        notificationService.deleteNotification(id);
    }
}

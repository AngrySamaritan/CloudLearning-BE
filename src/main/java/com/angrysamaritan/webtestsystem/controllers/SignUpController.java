package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.user.SignUpReq;
import com.angrysamaritan.webtestsystem.exceptions.BadSignUpRequest;
import com.angrysamaritan.webtestsystem.services.implementations.GroupServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.MailServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import com.angrysamaritan.webtestsystem.services.MailService;
import com.angrysamaritan.webtestsystem.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@Api
public class SignUpController {

    @Value("${app.mail.subject.confirmation}")
    private String confirmationSubject;

    private final UserService userService;
    private final MailService mailService;

    private final GroupServiceImpl groupServiceImpl;

    public SignUpController(UserServiceImpl userService, MailServiceImpl mailService,
                            GroupServiceImpl groupServiceImpl) {
        this.userService = userService;
        this.mailService = mailService;
        this.groupServiceImpl = groupServiceImpl;
    }

    @PostMapping(value = "/sign_up")
    public long postSignUp(@RequestBody @Valid SignUpReq signUpReq,
                           Errors errors)
            throws Exception {
        if (!errors.hasErrors()) {

            String confirmationCode = UUID.randomUUID().toString();

            long userId = userService.registerNewAccount(signUpReq, confirmationCode,
                    groupServiceImpl.getGroupByCode(signUpReq.getGroup()));
            mailService.sendMail(userId, confirmationSubject,
                    userService.getHtmlLetter(signUpReq.getConfirmationUrl() + confirmationCode));
            return 1L;
        } else {
            throw new BadSignUpRequest(errors);
        }
    }

    @PostMapping("/confirm/{code}")
    public long confirm(@PathVariable String code) throws Exception {
        userService.activateUserByCode(code);
        return 1L;
    }
}
package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.ChatDto;
import com.angrysamaritan.webtestsystem.dto.MessageDto;
import com.angrysamaritan.webtestsystem.facades.ChatFacade;
import com.angrysamaritan.webtestsystem.services.ChatService;
import com.angrysamaritan.webtestsystem.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.angrysamaritan.webtestsystem.config.KeyWords.CHATS;

@RestController
@RequestMapping(CHATS)
public class ChatController {

    private final ChatService chatService;
    private final UserService userService;
    private final ChatFacade chatFacade;


    public ChatController(ChatService chatService, UserService userService, ChatFacade chatFacade) {
        this.chatService = chatService;
        this.userService = userService;
        this.chatFacade = chatFacade;
    }

    @PutMapping
    public String createChat(@RequestBody ChatDto chatDto, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        return chatService.createChat(chatDto, userId);
    }

    @GetMapping
    public List<ChatDto> getAllChats(Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        return chatService.getChats(userId);
    }

    @GetMapping("/{chatId}")
    public List<MessageDto> getChat(Principal principal, @PathVariable String chatId, @RequestParam int page) {
        long userId = userService.getUserIdByName(principal.getName());
        return chatService.getMessages(chatId, userId, page);
    }

    @PostMapping("/{chatId}")
    public void sendMessage(@RequestBody MessageDto messageDto, @PathVariable String chatId, Principal principal) {
        long senderId = userService.getUserIdByName(principal.getName());
        chatFacade.sendMessage(messageDto, chatId, senderId);
    }
}

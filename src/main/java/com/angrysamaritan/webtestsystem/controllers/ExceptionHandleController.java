package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.ErrorsDto;
import com.angrysamaritan.webtestsystem.exceptions.BadRequestException;
import com.angrysamaritan.webtestsystem.exceptions.BadSignUpRequest;
import com.angrysamaritan.webtestsystem.exceptions.CodeNotExistException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandleController {

    @ExceptionHandler({BadRequestException.class, BadSignUpRequest.class, CodeNotExistException.class})
    public ResponseEntity<ErrorsDto> handleBadRequest(BadRequestException exception) {
        ErrorsDto errorsDto = exception.getErrorsDto();
        return new ResponseEntity<>(errorsDto, HttpStatus.BAD_REQUEST);
    }

}

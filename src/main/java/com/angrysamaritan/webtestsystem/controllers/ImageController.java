package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.model.Image;
import com.angrysamaritan.webtestsystem.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.angrysamaritan.webtestsystem.config.KeyWords.IMAGES;

@RestController
@RequestMapping(IMAGES)
public class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping
    public String upload(@RequestParam(value = "file", required = false) MultipartFile image) throws IOException {
        if (image != null) {
            return imageService.upload(image);
        } else return null;
    }

    @GetMapping
    public void get(@RequestParam(name = "id") String id, HttpServletResponse response) throws IOException {
        Image image = imageService.download(id);
        response.getOutputStream().write(image.getBytes());
    }
}

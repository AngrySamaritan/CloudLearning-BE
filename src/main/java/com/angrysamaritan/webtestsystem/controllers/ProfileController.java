package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.results.TestResultInfo;
import com.angrysamaritan.webtestsystem.dto.user.ProfileEditDto;
import com.angrysamaritan.webtestsystem.dto.user.ProfileDtoResp;
import com.angrysamaritan.webtestsystem.exceptions.AccessException;
import com.angrysamaritan.webtestsystem.facades.UserFacade;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.services.implementations.ProfileServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import com.angrysamaritan.webtestsystem.services.ProfileService;
import com.angrysamaritan.webtestsystem.services.SearchService;
import com.angrysamaritan.webtestsystem.services.TestResultService;
import com.angrysamaritan.webtestsystem.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.angrysamaritan.webtestsystem.config.KeyWords.*;

@RestController
@Api
@RequestMapping(USERS)
public class ProfileController {

    private final UserService userService;
    private final ProfileService profileService;
    private final TestResultService testResultService;
    private final SearchService searchService;
    private final UserFacade userFacade;

    @Autowired
    public ProfileController(UserServiceImpl userService, ProfileServiceImpl profileService,
                             TestResultService testResultService, SearchService searchService, UserFacade userFacade) {
        this.userService = userService;
        this.profileService = profileService;
        this.testResultService = testResultService;
        this.searchService = searchService;
        this.userFacade = userFacade;
    }

    @GetMapping("/{id}")
    public ProfileDtoResp getProfile(@PathVariable long id) {
        return profileService.getProfile(id);
    }

    @GetMapping(ME)
    public ProfileDtoResp getAuthorizedProfile(Principal principal) {
        var authorizedUserId = userService.getUserIdByName(principal.getName());
        return profileService.getProfile(authorizedUserId);
    }

    @PatchMapping("/{id}")
    public void postEditProfile(@RequestBody ProfileEditDto data, Principal principal, @PathVariable long id) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userId == id || userService.hasRole(userId, Role.Names.ROLE_ADMIN)) {
            profileService.editProfile(data, id);
        } else {
            throw new AccessException("No rights to edit this profile");
        }
    }

    @GetMapping(RESULTS + "/{userId}")
    public List<TestResultInfo> getResultList(@PathVariable long userId){
        return testResultService.getResultList(userId);
    }

    @GetMapping(SEARCH)
    public List<ProfileDtoResp> searchUser(@RequestParam("query") String searchQuery) {
        return searchService.searchUser(searchQuery);
    }

    @PatchMapping("/{userId}" + SUBJECTS)
    public void assigneeSubjects(Principal principal, @PathVariable long userId, @RequestBody List<Integer> subjectsId) {
        long adminId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(adminId, Role.Names.ROLE_ADMIN)) {
            userFacade.assigneeSubjects(userId, subjectsId, adminId);
        } else {
            throw new AccessException("No rights to assign user to subjects");
        }
    }
}

package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.group.GroupInfoResp;
import com.angrysamaritan.webtestsystem.dto.group.GroupUsersResp;
import com.angrysamaritan.webtestsystem.exceptions.GroupNotFoundException;
import com.angrysamaritan.webtestsystem.services.implementations.GroupServiceImpl;
import com.angrysamaritan.webtestsystem.services.GroupService;
import com.angrysamaritan.webtestsystem.services.SearchService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.angrysamaritan.webtestsystem.config.KeyWords.GROUPS;
import static com.angrysamaritan.webtestsystem.config.KeyWords.SEARCH;

@RestController
@RequestMapping(GROUPS)
@Api
public class GroupController {

    private final GroupService groupService;
    private final SearchService searchService;

    public GroupController(GroupServiceImpl groupService, SearchService searchService) {
        this.groupService = groupService;
        this.searchService = searchService;
    }

    @GetMapping("/{id}")
    public GroupUsersResp getGroup(@PathVariable int id) throws GroupNotFoundException {
        return groupService.getUsersList(id);
    }

    @GetMapping()
    public List<GroupInfoResp> getGroups() {
        return groupService.getGroups();
    }

    @GetMapping(SEARCH)
    public List<GroupInfoResp> searchGroup(@RequestParam("query") String searchQuery) {
        return searchService.searchGroup(searchQuery);
    }
}

package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.user.PasswordChangeRequest;
import com.angrysamaritan.webtestsystem.exceptions.CodeNotExistException;
import com.angrysamaritan.webtestsystem.exceptions.RequestValidationException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.repositories.PasswordChangeInfoRepository;
import com.angrysamaritan.webtestsystem.services.implementations.MailServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.PasswordChangeServiceImpl;
import com.angrysamaritan.webtestsystem.services.MailService;
import com.angrysamaritan.webtestsystem.services.PasswordChangeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

@RestController
@Api
public class PasswordChangeController {

    private final PasswordChangeInfoRepository passwordChangeInfoRepository;

    private final PasswordChangeService passwordChangeService;

    private final MailService mailService;

    @Autowired
    public PasswordChangeController(PasswordChangeInfoRepository passwordChangeInfoRepository,
                                    PasswordChangeServiceImpl passwordChangeService, MailServiceImpl mailService) {
        this.passwordChangeInfoRepository = passwordChangeInfoRepository;
        this.passwordChangeService = passwordChangeService;
        this.mailService = mailService;
    }

    @PostMapping("/password/change")
    public long requestPasswordChanging(
            @RequestParam("account") String usernameOrEmail,
            @RequestParam("confirmationUrl") String url) throws UserNotFoundException, MessagingException {

        var code = passwordChangeService.savePasswordChangeInfo(usernameOrEmail);
        mailService.sendMail(usernameOrEmail, "Change your password on Cloud Learning",
                passwordChangeService.getHtmlLetter(url + code));

        return 1L;
    }

    @GetMapping("/password/checkCode/{code}")
    public String checkCode(@PathVariable String code) {
        String username = passwordChangeInfoRepository.checkCode(code);
        if (username == null) {
            throw new CodeNotExistException("Code " + code + " not exist");
        }
        return username;
    }

    @PatchMapping("/password/change/{code}")
    public void changePassword(@PathVariable String code,
                               @RequestBody @Valid PasswordChangeRequest request, Errors errors) {
        if (errors.hasErrors()) {
            throw new RequestValidationException(errors);
        }
        passwordChangeService.changePassword(code, request.getPassword());
    }
}

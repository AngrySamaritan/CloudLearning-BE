package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.FullTestDto;
import com.angrysamaritan.webtestsystem.dto.results.TestResultDto;
import com.angrysamaritan.webtestsystem.dto.test.TestDtoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.*;
import com.angrysamaritan.webtestsystem.facades.TestCheckingFacade;
import com.angrysamaritan.webtestsystem.facades.TestCheckingFacadeImpl;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.services.*;
import com.angrysamaritan.webtestsystem.services.implementations.AccessManageServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.TestCreateServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.TestServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import static com.angrysamaritan.webtestsystem.config.KeyWords.*;

@Api
@RestController
@RequestMapping(TESTS)
public class TestController {

    private final TestCreateService testCreateService;
    private final TestService testService;
    private final UserService userService;
    private final AccessManageService accessManageService;
    private final TestCheckingFacade testCheckingFacade;
    private final TestResultService testResultService;
    private final SearchService searchService;

    private final SubjectService subjectService;

    @Autowired
    public TestController(TestCreateServiceImpl testCreateService,
                          TestServiceImpl testService, UserServiceImpl userService,
                          AccessManageServiceImpl accessManageServiceImpl, TestCheckingFacadeImpl testCheckingFacade,
                          TestResultService testResultService, SubjectService subjectService,
                          SearchService searchService) {
        this.testCreateService = testCreateService;
        this.testService = testService;
        this.userService = userService;
        this.accessManageService = accessManageServiceImpl;
        this.testCheckingFacade = testCheckingFacade;
        this.testResultService = testResultService;
        this.subjectService = subjectService;
        this.searchService = searchService;
    }

    @PostMapping("")
    public void createTest(@RequestBody @Valid FullTestDto test, Errors errors, Principal principal) {
        if (errors.hasErrors()) throw new BadRequestException("Test is incorrect!", errors);
        long userId = userService.getUserIdByName(principal.getName());
        subjectService.isModer(test.getSubjectId(), userId);
        testCreateService.saveOrUpdateTest(test, userId);
    }

    @GetMapping("/{testId}")
    public TestDtoResp getTest(@PathVariable long testId, Principal principal) throws TestNotFoundException,
            NoAccessToTestException {
        long userId = userService.getUserIdByName(principal.getName());
        accessManageService.checkAccess(testId, userId);
        accessManageService.invalidateAccess(testId, userId);
        accessManageService.startSession(testId, userId);
        return testService.getTest(testId);
    }

    @GetMapping("/{testId}" + FULL)
    public FullTestDto getTestForEditing(@PathVariable long testId, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (!testService.isAuthor(testId, userId)) {
            throw new NoAccessToTestException(testId);
        }
        return testService.getTestForEditing(testId);
    }

    @PostMapping("/{testId}" + CHECK)
    public long checkTest(@PathVariable Long testId, @RequestBody Map<Long, Object> answers, Principal principal)
            throws TestNotFoundException, QuestionNotFoundException, NoAccessToTestException {
        long userId = userService.getUserIdByName(principal.getName());
        return testCheckingFacade.checkTest(testId, answers, userId);
    }

    @GetMapping("/{testId}" + BRIEF)
    public TestInfoResp getTestInfo(@PathVariable long testId, Principal principal) throws TestNotFoundException {
        long userId = userService.getUserIdByName(principal.getName());
        accessManageService.checkAccess(testId, userId);
        return testService.getTestInfo(testId);
    }

    @GetMapping(RESULTS + "/{resultId}")
    public TestResultDto getResult(@PathVariable Long resultId, Principal principal) throws Exception {
        return testResultService.getTestResult(resultId, userService.getUserIdByName(principal.getName()));
    }

    @DeleteMapping("/{id}")
    public void markForDelete(@PathVariable long id, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(userId, Role.Names.ROLE_ADMIN) || testService.isAuthor(id, userId)) {
            testService.markForDelete(id);
        }
    }

    @GetMapping(SEARCH)
    public List<TestInfoResp> searchTest(@RequestParam("query") String searchQuery) throws TooShortQueryException {
        return searchService.searchTest(searchQuery);
    }
}

package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.SubjectInfoResp;
import com.angrysamaritan.webtestsystem.dto.test.TestInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.AccessException;
import com.angrysamaritan.webtestsystem.facades.SubjectFacade;
import com.angrysamaritan.webtestsystem.model.Role;
import com.angrysamaritan.webtestsystem.services.implementations.SubjectServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.TestServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import com.angrysamaritan.webtestsystem.services.SubjectService;
import com.angrysamaritan.webtestsystem.services.TestService;
import com.angrysamaritan.webtestsystem.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import static com.angrysamaritan.webtestsystem.config.KeyWords.SUBJECTS;
import static com.angrysamaritan.webtestsystem.config.KeyWords.USERS;

@RestController
@Api
@RequestMapping()
public class SubjectController {

    private final SubjectService subjectService;
    private final SubjectFacade subjectFacade;
    private final UserService userService;
    private final TestService testService;

    public SubjectController(SubjectServiceImpl subjectService, SubjectFacade subjectFacade, UserServiceImpl userService, TestServiceImpl testService) {
        this.subjectService = subjectService;
        this.subjectFacade = subjectFacade;
        this.userService = userService;
        this.testService = testService;
    }


    @GetMapping("/subject/id{subjectId}")
    public SubjectInfoResp getSubject(@PathVariable int subjectId) {
        return subjectService.getSubjectInfo(subjectId);
    }

    @Deprecated
    @GetMapping("/subject/id{subjectId}/createTest")
    public boolean hasTestCreatingAccess(@PathVariable int subjectId, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        return subjectService.isModer(subjectId, userId) || userService.hasRole(userId, Role.Names.ROLE_ADMIN);
    }


    @GetMapping(SUBJECTS)
    public List<SubjectInfoResp> getAllSubjects() {
        return subjectService.getAllSubjects();
    }

    @GetMapping("/subject/id{subjectId}/getAllowedTests")
    public List<TestInfoResp> getAllowedTests(@PathVariable int subjectId, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        return testService.getAllTests(subjectId, userId,
                userService.hasRole(userId, Role.Names.ROLE_ADMIN),
                subjectService.isModer(subjectId, userId));
    }

    @DeleteMapping(SUBJECTS + "/{id}")
    public void deleteSubject(@PathVariable int id, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(userId, Role.Names.ROLE_ADMIN)) {
            subjectService.deleteSubject(id);
        } else {
            throw new AccessException("Not enough rights to remove subjects");
        }
    }

    @PatchMapping(SUBJECTS + "/{id}")
    public void renameSubject(@PathVariable int id, @RequestBody String name, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(userId, Role.Names.ROLE_ADMIN)) {
            subjectService.renameSubject(id, name);
        } else {
            throw new AccessException("Not enough rights to remove subjects");
        }
    }

    @PutMapping(SUBJECTS)
    public void addSubject(@RequestBody String name, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(userId, Role.Names.ROLE_ADMIN)) {
            subjectService.createSubject(name);
        } else {
            throw new AccessException("Not enough rights to remove subjects");
        }
    }

    @PatchMapping(SUBJECTS + "/{id}" + USERS)
    public void patchModers(@RequestBody List<Long> userIds, @PathVariable int id, Principal principal) {
        long userId = userService.getUserIdByName(principal.getName());
        if (userService.hasRole(userId, Role.Names.ROLE_ADMIN)) {
            Set<Long> usersToCheck = subjectFacade.patchModers(id, userIds, userId);
            userService.checkRoles(usersToCheck);
        } else {
            throw new AccessException("Not enough rights to remove subjects");
        }
    }

}

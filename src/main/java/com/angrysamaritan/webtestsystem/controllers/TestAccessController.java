package com.angrysamaritan.webtestsystem.controllers;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.exceptions.NoAccessToTestException;
import com.angrysamaritan.webtestsystem.exceptions.TestNotFoundException;
import com.angrysamaritan.webtestsystem.facades.TestAccessFacade;
import com.angrysamaritan.webtestsystem.services.implementations.AccessManageServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.UserServiceImpl;
import com.angrysamaritan.webtestsystem.services.AccessManageService;
import com.angrysamaritan.webtestsystem.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static com.angrysamaritan.webtestsystem.config.KeyWords.*;

@RestController
@RequestMapping(TESTS + "/{testId}" + MANAGEMENT)
@Api
public class TestAccessController {

    private final AccessManageService accessManageService;

    private final UserService userService;

    private final TestAccessFacade testAccessFacade;

    public TestAccessController(AccessManageServiceImpl accessManageServiceImpl, UserServiceImpl userService, TestAccessFacade testAccessFacade) {
        this.accessManageService = accessManageServiceImpl;
        this.userService = userService;
        this.testAccessFacade = testAccessFacade;
    }

    @GetMapping(ACCESS)
    public long checkManagementAccess(@PathVariable long testId, Principal principal)
            throws TestNotFoundException, NoAccessToTestException {
        long userId = userService.getUserIdByName(principal.getName());
        accessManageService.hasAccessToManageTest(testId, userId);
        return 1L;
    }


    @GetMapping(USERS)
    public List<UserInfoResp> accessedUsersList(@PathVariable long testId, Principal principal) throws TestNotFoundException, NoAccessToTestException {
        accessManageService.hasAccessToManageTest(testId, userService.getUserIdByName(principal.getName()));
        return accessManageService.getAllowedUsers(testId);
    }

    @PatchMapping(USERS)
    public long patchAllowedUsers(@PathVariable long testId, @RequestBody List<Long> accessedUsersId, Principal principal)
            throws TestNotFoundException, NoAccessToTestException {
        long userId = userService.getUserIdByName(principal.getName());
        testAccessFacade.grantAccess(userId, testId, accessedUsersId);
        return 1L;
    }

}

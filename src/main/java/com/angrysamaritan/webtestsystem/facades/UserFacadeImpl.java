package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.services.NotificationService;
import com.angrysamaritan.webtestsystem.services.UserService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class UserFacadeImpl implements UserFacade {

    private final UserService userService;
    private final NotificationService notificationService;

    public UserFacadeImpl(UserService userService, NotificationService notificationService) {
        this.userService = userService;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public void assigneeSubjects(long userId, List<Integer> subjectsId, long adminId) {
        userService.assigneeSubjects(userId, subjectsId);
        notificationService.sendSubjectAssigneeChangedNotification(userId, adminId);
    }
}

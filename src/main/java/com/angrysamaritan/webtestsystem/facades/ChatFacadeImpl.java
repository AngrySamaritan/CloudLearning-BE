package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.dto.MessageDto;
import com.angrysamaritan.webtestsystem.exceptions.ChatNotFoundException;
import com.angrysamaritan.webtestsystem.model.Chat;
import com.angrysamaritan.webtestsystem.repositories.ChatRepository;
import com.angrysamaritan.webtestsystem.services.ChatService;
import com.angrysamaritan.webtestsystem.services.NotificationService;
import org.springframework.stereotype.Component;

@Component
public class ChatFacadeImpl implements ChatFacade {

    private final ChatService chatService;
    private final NotificationService notificationService;
    private final ChatRepository chatRepository;

    public ChatFacadeImpl(ChatService chatService, NotificationService notificationService, ChatRepository chatRepository) {
        this.chatService = chatService;
        this.notificationService = notificationService;
        this.chatRepository = chatRepository;
    }

    @Override
    public void sendMessage(MessageDto messageDto, String chatId, long senderId) {
        Chat chat = chatRepository.findById(chatId).orElseThrow(() -> new ChatNotFoundException(chatId));
        chatService.sendMessage(messageDto, chat, senderId);
        chat.getParticipants().stream().filter(u -> u.getUserId() != senderId)
                .forEach(p -> notificationService.newMessagesNotification(p.getUserId(), chat));
    }

}

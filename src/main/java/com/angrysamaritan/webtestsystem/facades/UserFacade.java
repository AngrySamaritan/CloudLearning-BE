package com.angrysamaritan.webtestsystem.facades;

import java.util.List;

public interface UserFacade {

    void assigneeSubjects(long userId, List<Integer> subjectsId, long adminId);

}

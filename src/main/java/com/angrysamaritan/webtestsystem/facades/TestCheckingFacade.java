package com.angrysamaritan.webtestsystem.facades;

import java.util.Map;

public interface TestCheckingFacade {
    long checkTest(long testId, Map<Long, Object> answers, long userId);
}

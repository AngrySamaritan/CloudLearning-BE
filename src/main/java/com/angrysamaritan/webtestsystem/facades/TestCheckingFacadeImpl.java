package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.services.implementations.AccessManageServiceImpl;
import com.angrysamaritan.webtestsystem.services.implementations.TestCompleteServiceImpl;
import com.angrysamaritan.webtestsystem.services.AccessManageService;
import com.angrysamaritan.webtestsystem.services.TestCompleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Component
public class TestCheckingFacadeImpl implements TestCheckingFacade {

    private final AccessManageService accessManageService;
    private final TestCompleteService testCompleteService;

    @Autowired
    public TestCheckingFacadeImpl(AccessManageServiceImpl accessManageService, TestCompleteServiceImpl testCompleteService) {
        this.accessManageService = accessManageService;
        this.testCompleteService = testCompleteService;
    }

    @Override
    @Transactional
    public long checkTest(long testId, Map<Long, Object> answers, long userId) {
        accessManageService.testCheckingAccess(testId, userId);
        return testCompleteService.checkTest(testId, answers, userId);
    }
}

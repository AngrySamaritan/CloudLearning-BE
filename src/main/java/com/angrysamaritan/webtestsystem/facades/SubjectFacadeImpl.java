package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.exceptions.SubjectNotFoundException;
import com.angrysamaritan.webtestsystem.model.Subject;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.SubjectRepository;
import com.angrysamaritan.webtestsystem.services.NotificationService;
import com.angrysamaritan.webtestsystem.services.SubjectService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class SubjectFacadeImpl implements SubjectFacade {

    private final SubjectService subjectService;
    private final SubjectRepository subjectRepository;
    private final NotificationService notificationService;

    public SubjectFacadeImpl(SubjectService subjectService,
                             SubjectRepository subjectRepository,
                             NotificationService notificationService) {
        this.subjectService = subjectService;
        this.subjectRepository = subjectRepository;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public Set<Long> patchModers(int id, List<Long> userIds, long adminId) {
        Subject subject = subjectRepository.findById(id).orElseThrow(() -> new SubjectNotFoundException(id));
        List<Long> oldUsersIds = subject.getTeachers().stream().map(User::getUserId).collect(Collectors.toList());
        Set<Long> usersToNotify = new HashSet<>(oldUsersIds);
        usersToNotify.addAll(userIds);
        subjectService.patchModers(id, userIds);
        usersToNotify = usersToNotify.stream().filter(u -> userIds.contains(u) && !oldUsersIds.contains(u) ||
                !userIds.contains(u) && oldUsersIds.contains(u)).collect(Collectors.toSet());
        usersToNotify.forEach(uid -> notificationService.sendSubjectAssigneeChangedNotification(uid, adminId));
        return usersToNotify;
    }
}

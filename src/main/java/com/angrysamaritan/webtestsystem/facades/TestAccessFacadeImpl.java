package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.dto.user.UserInfoResp;
import com.angrysamaritan.webtestsystem.services.AccessManageService;
import com.angrysamaritan.webtestsystem.services.NotificationService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TestAccessFacadeImpl implements TestAccessFacade {

    private final AccessManageService accessManageService;
    private final NotificationService notificationService;

    public TestAccessFacadeImpl(AccessManageService accessManageService, NotificationService notificationService) {
        this.accessManageService = accessManageService;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public void grantAccess(long userId, long testId, List<Long> accessedUsersId) {
        accessManageService.hasAccessToManageTest(testId, userId);
        List<Long> currentUsersId = accessManageService.getAllowedUsers(testId).stream().map(UserInfoResp::getId)
                .collect(Collectors.toList());
        accessManageService.patchUsersAccess(testId, accessedUsersId);
        accessedUsersId.stream().filter(id -> !currentUsersId.contains(id))
                .forEach(id -> notificationService.sendAccessToTestGrantedNotification(id, testId, userId));
    }
}

package com.angrysamaritan.webtestsystem.facades;

import com.angrysamaritan.webtestsystem.dto.MessageDto;

public interface ChatFacade {

    void sendMessage(MessageDto messageDto, String chatId, long senderId);

}

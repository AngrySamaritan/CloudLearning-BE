package com.angrysamaritan.webtestsystem.facades;

import java.util.List;
import java.util.Set;

public interface SubjectFacade {

    Set<Long> patchModers(int id, List<Long> userIds, long adminId);

}

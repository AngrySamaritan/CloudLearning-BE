package com.angrysamaritan.webtestsystem.facades;

import java.util.List;

public interface TestAccessFacade {

    void grantAccess(long userId, long testId, List<Long> accessedUsersId);

}

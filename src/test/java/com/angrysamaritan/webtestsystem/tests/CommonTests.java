package com.angrysamaritan.webtestsystem.tests;

import com.angrysamaritan.webtestsystem.dto.FullAnswerDto;
import com.angrysamaritan.webtestsystem.dto.FullQuestionDto;
import com.angrysamaritan.webtestsystem.dto.FullTestDto;
import com.angrysamaritan.webtestsystem.exceptions.SubjectNotFoundException;
import com.angrysamaritan.webtestsystem.model.QuestionType;
import com.angrysamaritan.webtestsystem.model.Subject;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.SubjectRepository;
import com.angrysamaritan.webtestsystem.repositories.TestRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.TestCreateService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"dev", "test"})
public class CommonTests {

    @Autowired
    TestCreateService testCreateService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    SubjectRepository subjectRepository;

    @Autowired
    TestRepository testRepository;

    User user;

    Subject subject;


    @Before
    public void before() {
        user = makeUser("TestUser1", "FName 1", "LName 1");
        user = userRepository.save(user);
        subject = new Subject();
        subject.setName("Test Subject #1");
        subject.setTeachers(Collections.singletonList(user));
        subject = subjectRepository.save(subject);
    }

    @After
    public void after() {
        testRepository.deleteAll();
        subjectRepository.deleteAll();
        userRepository.deleteAll();
    }

    private User makeUser(String username, String firstName, String lastName) {
        User user = new User();
        user.setProfileImage(null);
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Test
    public void saveOrUpdateTest() {

        User u2 = userRepository.save(makeUser("TestUser2", "FName 2", "LName 2"));

        FullTestDto testDto = FullTestDto.builder()
                .name("TName1")
                .testDescription("TDescription1")
                .duration(10)
                .subjectId(subject.getSubjectId())
                .build();


        List<FullQuestionDto> questionsDto = getQuestionDtos();
        testDto.setQuestions(questionsDto);
        testCreateService.saveOrUpdateTest(testDto, user.getUserId());
        Assert.assertTrue(testRepository.findAll().iterator().hasNext());

        testDto.setName("NewName");
        testDto.setTestId(testRepository.findAll().iterator().next().getTestId());
        testCreateService.saveOrUpdateTest(testDto, user.getUserId());
        Assert.assertTrue(testRepository.findAll().iterator().hasNext() &&
                testRepository.findAll().iterator().next().getName().equals("NewName"));

        testDto.setSubjectId(146);
        assertThrows(SubjectNotFoundException.class, () -> testCreateService.saveOrUpdateTest(testDto, user.getUserId()));

        com.angrysamaritan.webtestsystem.model.Test test = testRepository.findAll().iterator().next();

        test.setAuthor(null);
        testRepository.save(test);

        testDto.setSubjectId(subject.getSubjectId());
        testCreateService.saveOrUpdateTest(testDto, u2.getUserId());

        Assert.assertTrue(testRepository.findAll().iterator().hasNext() &&
                testRepository.findAll().iterator().next().getAuthor().getUserId() == u2.getUserId());
    }

    private List<FullQuestionDto> getQuestionDtos() {
        return new LinkedList<>() {{
            List<FullAnswerDto> answers = new LinkedList<>() {{
                add(FullAnswerDto.builder()
                        .answerText("Q1A1F")
                        .isRight(false)
                        .build()
                );
                add(FullAnswerDto.builder()
                        .answerText("Q1A2F")
                        .isRight(false)
                        .build()
                );
                add(FullAnswerDto.builder()
                        .answerText("Q1A3T")
                        .isRight(true)
                        .build()
                );
            }};
            add(FullQuestionDto.builder()
                    .questionType(QuestionType.SINGLE_SELECT_QUESTION)
                    .pointsAmount(10)
                    .answers(answers)
                    .build()
            );
            answers = new LinkedList<>() {{
                add(FullAnswerDto.builder()
                        .answerText("Q2A1T")
                        .isRight(true)
                        .build()
                );
                add(FullAnswerDto.builder()
                        .answerText("Q2A2F")
                        .isRight(false)
                        .build()
                );
                add(FullAnswerDto.builder()
                        .answerText("Q2A3T")
                        .isRight(true)
                        .build()
                );
            }};
            add(FullQuestionDto.builder()
                    .questionType(QuestionType.MULTIPLE_SELECT_QUESTION)
                    .pointsAmount(10)
                    .answers(answers)
                    .build()
            );
            answers = new LinkedList<>() {{
                add(FullAnswerDto.builder()
                        .answerText("Q3A1T")
                        .build()
                );
            }};
            add(FullQuestionDto.builder()
                    .questionType(QuestionType.WRITING_QUESTION)
                    .pointsAmount(10)
                    .answers(answers)
                    .build()
            );
        }};
    }


}

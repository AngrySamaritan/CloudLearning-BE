package com.angrysamaritan.webtestsystem.subjects;

import com.angrysamaritan.webtestsystem.exceptions.SubjectNotFoundException;
import com.angrysamaritan.webtestsystem.exceptions.UserNotFoundException;
import com.angrysamaritan.webtestsystem.model.Subject;
import com.angrysamaritan.webtestsystem.model.User;
import com.angrysamaritan.webtestsystem.repositories.SubjectRepository;
import com.angrysamaritan.webtestsystem.repositories.UserRepository;
import com.angrysamaritan.webtestsystem.services.SubjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles({"dev", "test"})
public class CommonTests {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SubjectRepository subjectRepository;

    @Autowired
    SubjectService subjectService;

    Subject subject;
    User user;

    @Before
    public void before() {
        user = makeUser("TestUser1", "FName 1", "LName 1");
        user = userRepository.save(user);
        subject = new Subject();
        subject.setName("Test Subject #1");
        subject.setTeachers(Collections.singletonList(user));
        subject = subjectRepository.save(subject);
    }

    private User makeUser(String username, String firstName, String lastName) {
        User user = new User();
        user.setProfileImage(null);
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    @Test
    public void isModerTest() {

        assertThrows(SubjectNotFoundException.class, () -> subjectService.isModer(12345, user.getUserId()));

        assertThrows(UserNotFoundException.class, () -> subjectService.isModer(subject.getSubjectId(), 12345));

        Assert.assertTrue(subjectService.isModer(subject.getSubjectId(), user.getUserId()));

        Assert.assertFalse(subjectService.isModer(subject.getSubjectId(), userRepository.save(
                makeUser("TestUSer2", "FName2", "LName2"))
                .getUserId()));
    }

}
